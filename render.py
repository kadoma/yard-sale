# -*- coding: utf-8 -*-
from math import ceil
from os import path
from tqdm import tqdm
from utils import chunks
import config
import urllib

class LotRenderingError(Exception):
	def __init__(self, page_number, lots_chunk, message):
		self.page_number = page_number
		self.lots_chunk = lots_chunk
		self.message = message
		super().__init__(message)

class BidRenderingError(Exception):
	def __init__(self, buyer_number, bids, message):
		self.buyer_number = buyer_number
		self.bids = bids
		self.message = message
		super().__init__(message)

class BuyersIndexRenderingError(Exception):
	def __init__(self, buyer_numbers, message):
		self.buyer_numbers = buyer_numbers
		self.message = message
		super().__init__(message)

class BuyersReducedRenderingError(Exception):
	def __init__(self, buyer_numbers, use_thumbnails, message):
		self.buyer_numbers = buyer_numbers
		self.use_thumbnails = use_thumbnails
		self.message = message
		super().__init__(message)

_lots_page_template = None
_buyer_page_template = None
_buyer_reduced_page_template = None
_buyer_reduced_thumbs_page_template = None
_buyers_index_page_template = None
with open(path.join('docs', config.LOTS_MD_TEMPLATE_FILE)) as template_file:
	_lots_page_template = template_file.read()
with open(path.join('docs', config.BUYER_MD_TEMPLATE_FILE)) as template_file:
	_buyer_page_template = template_file.read()
with open(path.join('docs', config.BUYERS_INDEX_MD_TEMPLATE_FILE)) as template_file:
	_buyers_index_page_template = template_file.read()
with open(path.join('docs', config.BUYER_REDUCED_MD_TEMPLATE_FILE)) as template_file:
	_buyer_reduced_page_template = template_file.read()
with open(path.join('docs', config.BUYER_REDUCED_THUMBS_MD_TEMPLATE_FILE)) as template_file:
	_buyer_reduced_thumbs_page_template = template_file.read()

def _format_price(x):
	if x is None:
		return '-'
	return '${:.2f} USD'.format(x)

def _render_thumbnail(lot):
	full_res_href = lot.image_href
	thumb_href = urllib.parse.urljoin(config.THUMBS_DIRECTORY_URL, lot.get_thumb_name() + '.jpg')
	return '<a href="{}"><img src="{}"/></a>'.format(full_res_href, thumb_href)

def _render_lot(lot):
	thumb_cell = _render_thumbnail(lot)
	max_bid = _format_price(lot.max_bid)
	max_bidder_number = '-' if lot.max_bidder_number is None else lot.max_bidder_number
	return '| Lot {} | {} | Buyer {} | Bid: {} |'.format(lot.id, thumb_cell, max_bidder_number, max_bid)

def _get_lots_page_md_filename(page_number):
	if page_number == 1:
		return 'index.md'
	else:
		suffix = '-{}'.format(page_number)
		return 'items' + suffix + '.md'

def _render_page_prev_button(current_page_number):
	prev_page_number = current_page_number - 1
	content = 'Prev'
	if prev_page_number < 1:
		return content
	else:
		md_file = _get_lots_page_md_filename(prev_page_number)
		return '[{}]({})'.format(content, md_file)

def _render_page_next_button(current_page_number, pages_count):
	next_page_number = current_page_number + 1
	content = 'Next'
	if next_page_number > pages_count:
		return content
	else:
		md_file = _get_lots_page_md_filename(next_page_number)
		return '[{}]({})'.format(content, md_file)

def _render_page_nav_button(target_page_number, current):
	content = '{}'.format(target_page_number)
	if current:
		return '**{}**'.format(content)
	else:
		md_file = _get_lots_page_md_filename(target_page_number)
		return '[{}]({})'.format(content, md_file)

def _render_page_nav(page_number, pages_count):
	page_nav_buttons = []
	if page_number > 1:
		page_nav_buttons.append(_render_page_prev_button(page_number))

	for i in range(1, pages_count + 1):
		page_nav_buttons.append(_render_page_nav_button(i, i == page_number))

	if page_number < pages_count:
		page_nav_buttons.append(_render_page_next_button(page_number, pages_count))

	return ' | '.join(page_nav_buttons)

def _generate_lot_page(chunk, page_number, pages_count):
	rendered_lots = []
	for lot in chunk:
		rendered_lots.append(_render_lot(lot))
	rendered_lots_joined = '\n'.join(rendered_lots)

	page_nav = _render_page_nav(page_number, pages_count)

	file_name = _get_lots_page_md_filename(page_number)
	file_path = path.join('docs', file_name)

	file_contents = _lots_page_template.format(
		lots=rendered_lots_joined,
		page_nav=page_nav
	)
	with open(file_path, 'w') as page_file:
		page_file.write(file_contents)

def generate_pages_for_lots(lots, progress_bar=True):
	lots_chunks = chunks(lots, config.LOTS_PER_PAGE)
	pages_count = ceil(len(lots) / config.LOTS_PER_PAGE)

	errors = []
	bar = tqdm(desc='Rendering lot pages', total=pages_count) if progress_bar else None

	for i, chunk in enumerate(lots_chunks):
		page_number = i + 1
		try:
			_generate_lot_page(chunk, page_number, pages_count)
		except Exception as err:
			errors.append(LotRenderingError(page_number, chunk, err))
		if progress_bar:
			bar.update()

	if progress_bar:
		bar.close()

	if len(errors) > 0:
		print('Errors ({}) have occured during lot pages renering:'.format(len(errors)))
		for error in errors:
			print('- Page #{}: {}'.format(error.page_number, error.message))
	else:
		print('Lot pages ({}) rendered successfully.'.format(pages_count))

def _get_buyer_page_md_filename(buyer_number):
	suffix = '-{}'.format(buyer_number)
	return 'buyer' + suffix + '.md'

def _get_buyers_index_page_md_filename():
	return 'buyers.md'

def _render_bid_status(bid_is_highest):
	if bid_is_highest:
		return '<span style="color:green">**Highest bid**</span>'
	else:
		return '<span style="color:firebrick">**Outbid**</span>'

def _render_bid(bid):
	thumb_cell = _render_thumbnail(bid.lot)
	your_bid = _format_price(bid.bid)
	max_bid = _format_price(bid.lot.max_bid)
	bid_status = _render_bid_status(bid.is_bid_highest())
	return '| Lot {} | {} | {} | Your bid: {} | Highest bid: {} |'.format(bid.lot.id, thumb_cell, bid_status, your_bid, max_bid)

def _generate_buyer_page(buyer_number, bids):
	rendered_bids = []
	for bid in bids:
		rendered_bids.append(_render_bid(bid))
	rendered_bids_joined = '\n'.join(rendered_bids)

	file_name = _get_buyer_page_md_filename(buyer_number)
	file_path = path.join('docs', file_name)

	file_contents = _buyer_page_template.format(
		buyer_number=buyer_number,
		bids=rendered_bids_joined
	)
	with open(file_path, 'w') as page_file:
		page_file.write(file_contents)

def _get_buyers_reduced_page_md_filename(use_thumbnails=False):
	if use_thumbnails:
		return 'buyers-reduced-thumbs.md'
	else:
		return 'buyers-reduced.md'

def _render_bid_reduced(bid):
	your_bid = _format_price(bid.bid)
	max_bid = _format_price(bid.lot.max_bid)
	bid_status = _render_bid_status(bid.is_bid_highest())
	return '| Lot {} | {} | Your bid: {} | Highest bid: {} |'.format(bid.lot.id, bid_status, your_bid, max_bid)

def _render_buyer_reduced(buyer_number, bids, use_thumbnails=False):
	rendered_bids = []
	for bid in bids:
		if use_thumbnails:
			rendered_bids.append(_render_bid(bid))
		else:
			rendered_bids.append(_render_bid_reduced(bid))
	rendered_bids_joined = '\n'.join(rendered_bids)

	template = _buyer_reduced_thumbs_page_template if use_thumbnails else _buyer_reduced_page_template

	rendered_buyer = template.format(
		buyer_number=buyer_number,
		bids=rendered_bids_joined
	)
	return rendered_buyer

def _generate_buyers_reduced_page(sorted_bidder_numbers, bids_by_bidder_number, use_thumbnails=False):
	rendered_buyers = []
	for bidder_number in sorted_bidder_numbers:
		bids = bids_by_bidder_number[bidder_number]
		rendered_buyers.append(_render_buyer_reduced(bidder_number, bids, use_thumbnails))

	file_name = _get_buyers_reduced_page_md_filename(use_thumbnails)
	file_path = path.join('docs', file_name)

	file_contents = '\n\n'.join(rendered_buyers)
	with open(file_path, 'w') as page_file:
		page_file.write(file_contents)

def _render_buyer_number_button(buyer_number):
	content = '{}'.format(buyer_number)
	md_file = _get_buyer_page_md_filename(buyer_number)
	return '[{}]({})'.format(content, md_file)

def _render_buyers_nav(buyer_numbers):
	buttons = map(lambda buyer_number: _render_buyer_number_button(buyer_number), buyer_numbers)
	return ' | '.join(buttons)

def _generate_buyers_index(buyer_numbers):
	buyers_nav = _render_buyers_nav(buyer_numbers)

	file_name = _get_buyers_index_page_md_filename()
	file_path = path.join('docs', file_name)

	file_contents = _buyers_index_page_template.format(
		buyers_nav=buyers_nav
	)
	with open(file_path, 'w') as page_file:
		page_file.write(file_contents)

def generate_pages_for_buyers(bids_by_bidder_number, progress_bar=True):
	buyer_numbers = list(bids_by_bidder_number.keys())
	buyer_numbers.sort()
	# The 3 accounts for: buyers index, buyers reduced and buyers reduced w\ thumbs
	pages_count = len(buyer_numbers) + 3

	errors = []
	bar = tqdm(desc='Rendering buyer pages', total=pages_count) if progress_bar else None

	for buyer_number in bids_by_bidder_number:
		bids = bids_by_bidder_number[buyer_number]
		try:
			_generate_buyer_page(buyer_number, bids)
		except Exception as err:
			errors.append(BidRenderingError(buyer_number, bids, err))
		if progress_bar:
			bar.update()

	try:
		_generate_buyers_index(buyer_numbers)
	except Exception as err:
		errors.append(BuyersIndexRenderingError(buyer_numbers, err))
	if progress_bar:
		bar.update()

	try:
		_generate_buyers_reduced_page(buyer_numbers, bids_by_bidder_number)
	except Exception as err:
		errors.append(BuyersReducedRenderingError(buyer_numbers, False, err))
	if progress_bar:
		bar.update()

	try:
		_generate_buyers_reduced_page(buyer_numbers, bids_by_bidder_number, use_thumbnails=True)
	except Exception as err:
		errors.append(BuyersReducedRenderingError(buyer_numbers, True, err))
	if progress_bar:
		bar.update()
		bar.close()

	if len(errors) > 0:
		print('Errors ({}) have occured during buyer pages renering:'.format(len(errors)))
		for error in errors:
			if isinstance(error, BidRenderingError):
				print('- Buyer #{}: {}'.format(error.buyer_number, error.message))
			elif isinstance(error, BuyersIndexRenderingError):
				print('- Buyers index: {}'.format(error.message))
			elif isinstance(error, BuyersReducedRenderingError) and error.use_thumbnails:
				print('- Buyers reduced (w\ thumbs): {}'.format(error.message))
			elif isinstance(error, BuyersReducedRenderingError) and not error.use_thumbnails:
				print('- Buyers reduced: {}'.format(error.message))
	else:
		print('Buyer pages ({}) rendered successfully.'.format(pages_count))

__all__ = ['LotRenderingError', 'BidRenderingError', 'generate_pages_for_lots', 'generate_pages_for_buyers']
