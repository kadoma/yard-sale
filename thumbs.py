# -*- coding: utf-8 -*-
from PIL import Image
from tqdm import tqdm
import config
import io
import os
import requests

class ImageDownloadError(Exception):
	pass

class ThumbnailGenerationError(Exception):
	def __init__(self, lot, message):
		self.lot = lot
		self.message = message
		super().__init__(message)

# May raise OSError on unsuccessful save
def _generate_thumb(lot):
	if (lot.image_href is None or lot.image_href == ''):
		return

	try:
		response = requests.get(lot.image_href)
		if response.status_code != 200:
			raise ImageDownloadError('{}: {}'.format(response.status_code, response.reason))
	except requests.exceptions.ConnectionError as err:
		raise ImageDownloadError(err)

	img = Image.open(io.BytesIO(response.content))

	aspect_ratio = img.width / img.height
	height = round(config.THUMBNAIL_HEIGHT)
	width = round(aspect_ratio * height)
	thumb_img = img.resize(( width, height ))

	thumb_file_path = os.path.join(config.THUMBS_DIRECTORY, lot.get_thumb_name() + '.jpg')
	thumb_img.save(thumb_file_path, quality=config.THUMBS_JPEG_QUALITY)

def generate_thumbs_for_lots(lots, progress_bar=True):
	errors = []
	bar = tqdm(desc='Generating thumbnails', total=len(lots)) if progress_bar else None

	for lot in lots:
		try:
			_generate_thumb(lot)
		except Exception as err:
			errors.append(ThumbnailGenerationError(lot, err))
		if progress_bar:
			bar.update()

	if progress_bar:
		bar.close()

	if len(errors) > 0:
		print('Errors ({}) have occured during thumbnail generation:'.format(len(errors)))
		for error in errors:
			print('- Lot #{}: {}'.format(error.lot.id, error.message))
	else:
		print('Thumbnails ({}) generated successfully.'.format(len(lots)))

__all__ = ['ThumbnailGenerationError', 'generate_thumbs_for_lots']
