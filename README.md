## Setup

The following prerequisites are required:

* Python 3: https://www.python.org/downloads/
* Git: https://git-scm.com/downloads

Ensure that you have a vaild SSH key saved as `%homepath%\.ssh\id_rsa`.

Afterwards, run `bin\Setup.bat`.