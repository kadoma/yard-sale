# Height of generated thumbnails given in px
THUMBNAIL_HEIGHT = 100

# The relative path to the Excel file, with respect to this project's directory
EXCEL_INPUT_FILE_PATH = 'AuctionData.xlsx'

# Which sheet contains the lots data
EXCEL_LOTS_SHEET_NAME = 'Lots'

# Which sheet contains the raw inputs
EXCEL_INPUTS_SHEET_NAME = 'Inps'

# How many lots to display on each page
LOTS_PER_PAGE = 100

# Relative path to the directory which will store thumbnails
THUMBS_DIRECTORY = 'thumbs'

# The URL of the publicly available thumbnails directory
THUMBS_DIRECTORY_URL = 'https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/'

# Quality value between 0 and 100. The lower, the smaller the size and quality.
THUMBS_JPEG_QUALITY = 80

# Name of the *.md template file (relative to the docs/ directory) for each lots page
LOTS_MD_TEMPLATE_FILE = 'items.template.md'

# Name of the *.md template file (relative to the docs/ directory) for the index of buyers
BUYERS_INDEX_MD_TEMPLATE_FILE = 'buyers.template.md'

# Name of the *.md template file (relative to the docs/ directory) for each individual buyer's page
BUYER_MD_TEMPLATE_FILE = 'buyer.template.md'

# Name of the *.md template file (relative to the docs/ directory) for a single buyer's entry in reduced form (no thumbnails, for the secret index)
BUYER_REDUCED_MD_TEMPLATE_FILE = 'buyer-reduced.template.md'

# Name of the *.md template file (relative to the docs/ directory) for a single buyer's entry in reduced form (yes thumbnails, for the secret index)
BUYER_REDUCED_THUMBS_MD_TEMPLATE_FILE = 'buyer-reduced-thumbs.template.md'
