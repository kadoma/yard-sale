# -*- coding: utf-8 -*-
import config
import openpyxl

lots = []
lotsById = {}

bids = []
currentBidsByBidderNumber = {}

class Lot:
	def __init__(self, id, max_bid, max_bidder_number, image_href):
		self.id = int(id)
		if max_bidder_number is None:
			self.max_bid = None
			self.max_bidder_number = None
		else:
			self.max_bid = float(max_bid)
			self.max_bidder_number = int(max_bidder_number)
		self.image_href = image_href

	def __repr__(self):
		return '<Lot: id={}, max_bid={}, max_bidder_number={}, image_href="{}">'.format(self.id, self.max_bid, self.max_bidder_number, self.image_href)

	def get_thumb_name(self):
		return str(self.id)

class Bid:
	def __init__(self, lot, bidder_number, bid):
		self.lot = lot
		self.bidder_number = bidder_number
		self.bid = bid

	def __repr__(self):
		return '<Bids: lot={}, bidder_number={}, bid={}, is_bid_highest()=>{}>'.format(self.lot, self.bidder_number, self.bid, self.is_bid_highest())

	def is_bid_highest(self):
		return self.lot.max_bidder_number == self.bidder_number

# Collect data from the *.xlsx file. data_only=True makes it, so that all the
#  formulae are evaluated.
workbook = openpyxl.load_workbook(config.EXCEL_INPUT_FILE_PATH, data_only=True)
lotsSheet = workbook[config.EXCEL_LOTS_SHEET_NAME]
bidsSheet = workbook[config.EXCEL_INPUTS_SHEET_NAME]

# min_row=2 to skip the header
for row in lotsSheet.iter_rows(min_row=2):
	id = row[0].value
	max_bid = row[5].value
	max_bidder_number = row[8].value
	image_href = row[9].value

	lot = Lot(id, max_bid, max_bidder_number, image_href)
	lots.append(lot)
	lotsById[lot.id] = lot

# Keep track of most recent (highest) bids of individual buyers for each lot
_most_recent_bids = {}
def _get_most_recent_bid(bidder_number, lot_id):
	if bidder_number in _most_recent_bids:
		bids_by_lot_id = _most_recent_bids[bidder_number]
		if lot_id in bids_by_lot_id:
			bid = bids_by_lot_id[lot_id]
			return bid
		else:
			_most_recent_bids[bidder_number][lot_id] = None
	else:
		_most_recent_bids[bidder_number] = {}

	return None

def _update_most_recent_bid(bid):
	last_bid = _get_most_recent_bid(bid.bidder_number, bid.lot.id)
	if last_bid is None or bid.bid > last_bid.bid:
		_most_recent_bids[bid.bidder_number][bid.lot.id] = bid
		return True
	else:
		return False

for row in bidsSheet.iter_rows(min_row=2):
	bidder_number = row[0].value
	lot_id = row[1].value
	bid_amount = row[2].value

	# Ignore empty rows
	if not bidder_number:
		continue

	lot = lotsById[lot_id]
	bid = Bid(lot, bidder_number, bid_amount)
	bids.append(bid)
	_update_most_recent_bid(bid)

# Collect the current bids with respect to bidder number
for bidder_number in _most_recent_bids:
	if bidder_number not in currentBidsByBidderNumber:
		currentBidsByBidderNumber[bidder_number] = []
	bids_by_lot_id = _most_recent_bids[bidder_number]
	for lot_id in bids_by_lot_id:
		bid = bids_by_lot_id[lot_id]
		currentBidsByBidderNumber[bidder_number].append(bid)

	# Sort by lot id for each buyer
	currentBidsByBidderNumber[bidder_number].sort(key=lambda bid: bid.lot.id)

__all__ = ['Lot', 'lots', 'lotsById', 'Bid', 'bids', 'currentBidsByBidderNumber']
