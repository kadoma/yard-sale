# -*- coding: utf-8 -*-
import data
import render

render.generate_pages_for_lots(data.lots)
render.generate_pages_for_buyers(data.currentBidsByBidderNumber)
