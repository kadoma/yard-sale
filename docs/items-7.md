# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 601 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/601.jpg"/></a> | Buyer - | Bid: - |
| Lot 602 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/602.jpg"/></a> | Buyer - | Bid: - |
| Lot 603 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/603.jpg"/></a> | Buyer - | Bid: - |
| Lot 604 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/604.jpg"/></a> | Buyer - | Bid: - |
| Lot 605 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/605.jpg"/></a> | Buyer - | Bid: - |
| Lot 606 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/606.jpg"/></a> | Buyer - | Bid: - |
| Lot 607 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/607.jpg"/></a> | Buyer - | Bid: - |
| Lot 608 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/608.jpg"/></a> | Buyer - | Bid: - |
| Lot 609 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/609.jpg"/></a> | Buyer - | Bid: - |
| Lot 610 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/610.jpg"/></a> | Buyer - | Bid: - |
| Lot 611 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/611.jpg"/></a> | Buyer - | Bid: - |
| Lot 612 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/612.jpg"/></a> | Buyer - | Bid: - |
| Lot 613 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/613.jpg"/></a> | Buyer - | Bid: - |
| Lot 614 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/614.jpg"/></a> | Buyer - | Bid: - |
| Lot 615 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/615.jpg"/></a> | Buyer - | Bid: - |
| Lot 616 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/616.jpg"/></a> | Buyer - | Bid: - |
| Lot 617 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/617.jpg"/></a> | Buyer - | Bid: - |
| Lot 618 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/618.jpg"/></a> | Buyer - | Bid: - |
| Lot 619 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/619.jpg"/></a> | Buyer - | Bid: - |
| Lot 620 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/620.jpg"/></a> | Buyer - | Bid: - |
| Lot 621 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/621.jpg"/></a> | Buyer - | Bid: - |
| Lot 622 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/622.jpg"/></a> | Buyer - | Bid: - |
| Lot 623 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/623.jpg"/></a> | Buyer - | Bid: - |
| Lot 624 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/624.jpg"/></a> | Buyer - | Bid: - |
| Lot 625 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/625.jpg"/></a> | Buyer - | Bid: - |
| Lot 626 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/626.jpg"/></a> | Buyer - | Bid: - |
| Lot 627 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/627.jpg"/></a> | Buyer - | Bid: - |
| Lot 628 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/628.jpg"/></a> | Buyer - | Bid: - |
| Lot 629 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/629.jpg"/></a> | Buyer - | Bid: - |
| Lot 630 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/630.jpg"/></a> | Buyer - | Bid: - |
| Lot 631 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/631.jpg"/></a> | Buyer - | Bid: - |
| Lot 632 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/632.jpg"/></a> | Buyer - | Bid: - |
| Lot 633 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/633.jpg"/></a> | Buyer - | Bid: - |
| Lot 634 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/634.jpg"/></a> | Buyer - | Bid: - |
| Lot 635 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/635.jpg"/></a> | Buyer - | Bid: - |
| Lot 636 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/636.jpg"/></a> | Buyer - | Bid: - |
| Lot 637 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/637.jpg"/></a> | Buyer - | Bid: - |
| Lot 638 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/638.jpg"/></a> | Buyer - | Bid: - |
| Lot 639 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/639.jpg"/></a> | Buyer - | Bid: - |
| Lot 640 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/640.jpg"/></a> | Buyer - | Bid: - |
| Lot 641 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/641.jpg"/></a> | Buyer - | Bid: - |
| Lot 642 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/642.jpg"/></a> | Buyer - | Bid: - |
| Lot 643 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/643.jpg"/></a> | Buyer - | Bid: - |
| Lot 644 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/644.jpg"/></a> | Buyer - | Bid: - |
| Lot 645 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/645.jpg"/></a> | Buyer - | Bid: - |
| Lot 646 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/646.jpg"/></a> | Buyer - | Bid: - |
| Lot 647 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/647.jpg"/></a> | Buyer - | Bid: - |
| Lot 648 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/648.jpg"/></a> | Buyer - | Bid: - |
| Lot 649 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/649.jpg"/></a> | Buyer - | Bid: - |
| Lot 650 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/650.jpg"/></a> | Buyer - | Bid: - |
| Lot 651 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/651.jpg"/></a> | Buyer - | Bid: - |
| Lot 652 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/652.jpg"/></a> | Buyer - | Bid: - |
| Lot 653 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/653.jpg"/></a> | Buyer - | Bid: - |
| Lot 654 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/654.jpg"/></a> | Buyer - | Bid: - |
| Lot 655 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/655.jpg"/></a> | Buyer - | Bid: - |
| Lot 656 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/656.jpg"/></a> | Buyer - | Bid: - |
| Lot 657 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/657.jpg"/></a> | Buyer - | Bid: - |
| Lot 658 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/658.jpg"/></a> | Buyer - | Bid: - |
| Lot 659 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/659.jpg"/></a> | Buyer - | Bid: - |
| Lot 660 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/660.jpg"/></a> | Buyer - | Bid: - |
| Lot 661 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/661.jpg"/></a> | Buyer - | Bid: - |
| Lot 662 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/662.jpg"/></a> | Buyer - | Bid: - |
| Lot 663 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/663.jpg"/></a> | Buyer - | Bid: - |
| Lot 664 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/664.jpg"/></a> | Buyer - | Bid: - |
| Lot 665 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/665.jpg"/></a> | Buyer - | Bid: - |
| Lot 666 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/666.jpg"/></a> | Buyer - | Bid: - |
| Lot 667 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/667.jpg"/></a> | Buyer - | Bid: - |
| Lot 668 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/668.jpg"/></a> | Buyer - | Bid: - |
| Lot 669 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/669.jpg"/></a> | Buyer - | Bid: - |
| Lot 670 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/670.jpg"/></a> | Buyer - | Bid: - |
| Lot 671 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/671.jpg"/></a> | Buyer - | Bid: - |
| Lot 672 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/672.jpg"/></a> | Buyer - | Bid: - |
| Lot 673 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/673.jpg"/></a> | Buyer - | Bid: - |
| Lot 674 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/674.jpg"/></a> | Buyer - | Bid: - |
| Lot 675 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/675.jpg"/></a> | Buyer - | Bid: - |
| Lot 676 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/676.jpg"/></a> | Buyer - | Bid: - |
| Lot 677 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/677.jpg"/></a> | Buyer - | Bid: - |
| Lot 678 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/678.jpg"/></a> | Buyer - | Bid: - |
| Lot 679 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/679.jpg"/></a> | Buyer - | Bid: - |
| Lot 680 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/680.jpg"/></a> | Buyer - | Bid: - |
| Lot 681 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/681.jpg"/></a> | Buyer - | Bid: - |
| Lot 682 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/682.jpg"/></a> | Buyer - | Bid: - |
| Lot 683 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/683.jpg"/></a> | Buyer - | Bid: - |
| Lot 684 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/684.jpg"/></a> | Buyer - | Bid: - |
| Lot 685 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/685.jpg"/></a> | Buyer - | Bid: - |
| Lot 686 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/686.jpg"/></a> | Buyer - | Bid: - |
| Lot 687 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/687.jpg"/></a> | Buyer - | Bid: - |
| Lot 688 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/688.jpg"/></a> | Buyer - | Bid: - |
| Lot 689 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/689.jpg"/></a> | Buyer - | Bid: - |
| Lot 690 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/690.jpg"/></a> | Buyer - | Bid: - |
| Lot 691 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/691.jpg"/></a> | Buyer - | Bid: - |
| Lot 692 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/692.jpg"/></a> | Buyer - | Bid: - |
| Lot 693 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/693.jpg"/></a> | Buyer - | Bid: - |
| Lot 694 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/694.jpg"/></a> | Buyer - | Bid: - |
| Lot 695 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/695.jpg"/></a> | Buyer - | Bid: - |
| Lot 696 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/696.jpg"/></a> | Buyer - | Bid: - |
| Lot 697 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/697.jpg"/></a> | Buyer - | Bid: - |
| Lot 698 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/698.jpg"/></a> | Buyer - | Bid: - |
| Lot 699 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/699.jpg"/></a> | Buyer - | Bid: - |
| Lot 700 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/700.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-6.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | **7** | [8](items-8.md) | [9](items-9.md) | [10](items-10.md) | [Next](items-8.md)
