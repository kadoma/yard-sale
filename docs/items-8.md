# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 701 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/701.jpg"/></a> | Buyer - | Bid: - |
| Lot 702 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/702.jpg"/></a> | Buyer - | Bid: - |
| Lot 703 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/703.jpg"/></a> | Buyer - | Bid: - |
| Lot 704 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/704.jpg"/></a> | Buyer - | Bid: - |
| Lot 705 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/705.jpg"/></a> | Buyer - | Bid: - |
| Lot 706 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/706.jpg"/></a> | Buyer - | Bid: - |
| Lot 707 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/707.jpg"/></a> | Buyer - | Bid: - |
| Lot 708 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/708.jpg"/></a> | Buyer - | Bid: - |
| Lot 709 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/709.jpg"/></a> | Buyer - | Bid: - |
| Lot 710 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/710.jpg"/></a> | Buyer - | Bid: - |
| Lot 711 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/711.jpg"/></a> | Buyer - | Bid: - |
| Lot 712 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/712.jpg"/></a> | Buyer - | Bid: - |
| Lot 713 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/713.jpg"/></a> | Buyer - | Bid: - |
| Lot 714 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/714.jpg"/></a> | Buyer - | Bid: - |
| Lot 715 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/715.jpg"/></a> | Buyer - | Bid: - |
| Lot 716 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/716.jpg"/></a> | Buyer - | Bid: - |
| Lot 717 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/717.jpg"/></a> | Buyer - | Bid: - |
| Lot 718 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/718.jpg"/></a> | Buyer - | Bid: - |
| Lot 719 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/719.jpg"/></a> | Buyer - | Bid: - |
| Lot 720 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/720.jpg"/></a> | Buyer - | Bid: - |
| Lot 721 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/721.jpg"/></a> | Buyer - | Bid: - |
| Lot 722 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/722.jpg"/></a> | Buyer - | Bid: - |
| Lot 723 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/723.jpg"/></a> | Buyer - | Bid: - |
| Lot 724 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/724.jpg"/></a> | Buyer - | Bid: - |
| Lot 725 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/725.jpg"/></a> | Buyer - | Bid: - |
| Lot 726 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/726.jpg"/></a> | Buyer - | Bid: - |
| Lot 727 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/727.jpg"/></a> | Buyer - | Bid: - |
| Lot 728 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/728.jpg"/></a> | Buyer - | Bid: - |
| Lot 729 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/729.jpg"/></a> | Buyer - | Bid: - |
| Lot 730 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/730.jpg"/></a> | Buyer - | Bid: - |
| Lot 731 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/731.jpg"/></a> | Buyer - | Bid: - |
| Lot 732 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/732.jpg"/></a> | Buyer - | Bid: - |
| Lot 733 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/733.jpg"/></a> | Buyer - | Bid: - |
| Lot 734 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/734.jpg"/></a> | Buyer - | Bid: - |
| Lot 735 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/735.jpg"/></a> | Buyer - | Bid: - |
| Lot 736 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/736.jpg"/></a> | Buyer - | Bid: - |
| Lot 737 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/737.jpg"/></a> | Buyer - | Bid: - |
| Lot 738 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/738.jpg"/></a> | Buyer - | Bid: - |
| Lot 739 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/739.jpg"/></a> | Buyer - | Bid: - |
| Lot 740 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/740.jpg"/></a> | Buyer - | Bid: - |
| Lot 741 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/741.jpg"/></a> | Buyer - | Bid: - |
| Lot 742 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/742.jpg"/></a> | Buyer - | Bid: - |
| Lot 743 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/743.jpg"/></a> | Buyer - | Bid: - |
| Lot 744 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/744.jpg"/></a> | Buyer - | Bid: - |
| Lot 745 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/745.jpg"/></a> | Buyer - | Bid: - |
| Lot 746 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/746.jpg"/></a> | Buyer - | Bid: - |
| Lot 747 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/747.jpg"/></a> | Buyer - | Bid: - |
| Lot 748 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/748.jpg"/></a> | Buyer - | Bid: - |
| Lot 749 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/749.jpg"/></a> | Buyer - | Bid: - |
| Lot 750 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/750.jpg"/></a> | Buyer - | Bid: - |
| Lot 751 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/751.jpg"/></a> | Buyer - | Bid: - |
| Lot 752 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/752.jpg"/></a> | Buyer - | Bid: - |
| Lot 753 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/753.jpg"/></a> | Buyer - | Bid: - |
| Lot 754 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/754.jpg"/></a> | Buyer - | Bid: - |
| Lot 755 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/755.jpg"/></a> | Buyer - | Bid: - |
| Lot 756 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/756.jpg"/></a> | Buyer - | Bid: - |
| Lot 757 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/757.jpg"/></a> | Buyer - | Bid: - |
| Lot 758 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/758.jpg"/></a> | Buyer - | Bid: - |
| Lot 759 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/759.jpg"/></a> | Buyer - | Bid: - |
| Lot 760 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/760.jpg"/></a> | Buyer - | Bid: - |
| Lot 761 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/761.jpg"/></a> | Buyer - | Bid: - |
| Lot 762 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/762.jpg"/></a> | Buyer - | Bid: - |
| Lot 763 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/763.jpg"/></a> | Buyer - | Bid: - |
| Lot 764 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/764.jpg"/></a> | Buyer - | Bid: - |
| Lot 765 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/765.jpg"/></a> | Buyer - | Bid: - |
| Lot 766 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/766.jpg"/></a> | Buyer - | Bid: - |
| Lot 767 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/767.jpg"/></a> | Buyer - | Bid: - |
| Lot 768 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/768.jpg"/></a> | Buyer - | Bid: - |
| Lot 769 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/769.jpg"/></a> | Buyer - | Bid: - |
| Lot 770 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/770.jpg"/></a> | Buyer - | Bid: - |
| Lot 771 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/771.jpg"/></a> | Buyer - | Bid: - |
| Lot 772 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/772.jpg"/></a> | Buyer - | Bid: - |
| Lot 773 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/773.jpg"/></a> | Buyer - | Bid: - |
| Lot 774 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/774.jpg"/></a> | Buyer - | Bid: - |
| Lot 775 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/775.jpg"/></a> | Buyer - | Bid: - |
| Lot 776 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/776.jpg"/></a> | Buyer - | Bid: - |
| Lot 777 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/777.jpg"/></a> | Buyer - | Bid: - |
| Lot 778 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/778.jpg"/></a> | Buyer - | Bid: - |
| Lot 779 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/779.jpg"/></a> | Buyer - | Bid: - |
| Lot 780 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/780.jpg"/></a> | Buyer - | Bid: - |
| Lot 781 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/781.jpg"/></a> | Buyer - | Bid: - |
| Lot 782 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/782.jpg"/></a> | Buyer - | Bid: - |
| Lot 783 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/783.jpg"/></a> | Buyer - | Bid: - |
| Lot 784 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/784.jpg"/></a> | Buyer - | Bid: - |
| Lot 785 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/785.jpg"/></a> | Buyer - | Bid: - |
| Lot 786 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/786.jpg"/></a> | Buyer - | Bid: - |
| Lot 787 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/787.jpg"/></a> | Buyer - | Bid: - |
| Lot 788 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/788.jpg"/></a> | Buyer - | Bid: - |
| Lot 789 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/789.jpg"/></a> | Buyer - | Bid: - |
| Lot 790 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/790.jpg"/></a> | Buyer - | Bid: - |
| Lot 791 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/791.jpg"/></a> | Buyer - | Bid: - |
| Lot 792 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/792.jpg"/></a> | Buyer - | Bid: - |
| Lot 793 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/793.jpg"/></a> | Buyer - | Bid: - |
| Lot 794 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/794.jpg"/></a> | Buyer - | Bid: - |
| Lot 795 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/795.jpg"/></a> | Buyer - | Bid: - |
| Lot 796 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/796.jpg"/></a> | Buyer - | Bid: - |
| Lot 797 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/797.jpg"/></a> | Buyer - | Bid: - |
| Lot 798 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/798.jpg"/></a> | Buyer - | Bid: - |
| Lot 799 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/799.jpg"/></a> | Buyer - | Bid: - |
| Lot 800 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/800.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-7.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [7](items-7.md) | **8** | [9](items-9.md) | [10](items-10.md) | [Next](items-9.md)
