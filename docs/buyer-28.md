# Buyer 28's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 3 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot3.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/3.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $200.00 USD | Highest bid: $625.00 USD |
| Lot 6 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot6.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/6.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $3300.00 USD | Highest bid: $3300.00 USD |
| Lot 108 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot108.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/108.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $190.00 USD |
| Lot 168 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot168.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/168.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $600.00 USD | Highest bid: $600.00 USD |
| Lot 175 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot175.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/175.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $300.00 USD | Highest bid: $300.00 USD |
| Lot 177 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot177.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/177.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $1100.00 USD | Highest bid: $1100.00 USD |
