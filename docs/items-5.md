# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here to see check the status of bids where only the lots you have bid on are displayed**](buyers.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

[Prev](items-4.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | **5** | [6](items-6.md) | [Next](items-6.md)

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 401 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot401.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/401.jpg"/></a> | Buyer - | Bid: - |
| Lot 402 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot402.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/402.jpg"/></a> | Buyer - | Bid: - |
| Lot 403 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot403.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/403.jpg"/></a> | Buyer - | Bid: - |
| Lot 404 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot404.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/404.jpg"/></a> | Buyer - | Bid: - |
| Lot 405 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot405.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/405.jpg"/></a> | Buyer - | Bid: - |
| Lot 406 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot406.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/406.jpg"/></a> | Buyer - | Bid: - |
| Lot 407 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot407.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/407.jpg"/></a> | Buyer - | Bid: - |
| Lot 408 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot408.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/408.jpg"/></a> | Buyer - | Bid: - |
| Lot 409 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot409.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/409.jpg"/></a> | Buyer - | Bid: - |
| Lot 410 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot410.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/410.jpg"/></a> | Buyer - | Bid: - |
| Lot 411 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot411.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/411.jpg"/></a> | Buyer - | Bid: - |
| Lot 412 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot412.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/412.jpg"/></a> | Buyer - | Bid: - |
| Lot 413 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot413.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/413.jpg"/></a> | Buyer - | Bid: - |
| Lot 414 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot414.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/414.jpg"/></a> | Buyer - | Bid: - |
| Lot 415 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot415.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/415.jpg"/></a> | Buyer - | Bid: - |
| Lot 416 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot416.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/416.jpg"/></a> | Buyer - | Bid: - |
| Lot 417 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot417.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/417.jpg"/></a> | Buyer - | Bid: - |
| Lot 418 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot418.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/418.jpg"/></a> | Buyer - | Bid: - |
| Lot 419 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot419.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/419.jpg"/></a> | Buyer - | Bid: - |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | Buyer - | Bid: - |
| Lot 421 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot421.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/421.jpg"/></a> | Buyer - | Bid: - |
| Lot 422 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot422.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/422.jpg"/></a> | Buyer - | Bid: - |
| Lot 423 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot423.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/423.jpg"/></a> | Buyer - | Bid: - |
| Lot 424 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot424.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/424.jpg"/></a> | Buyer - | Bid: - |
| Lot 425 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot425.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/425.jpg"/></a> | Buyer - | Bid: - |
| Lot 426 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot426.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/426.jpg"/></a> | Buyer - | Bid: - |
| Lot 427 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot427.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/427.jpg"/></a> | Buyer - | Bid: - |
| Lot 428 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot428.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/428.jpg"/></a> | Buyer - | Bid: - |
| Lot 429 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot429.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/429.jpg"/></a> | Buyer - | Bid: - |
| Lot 430 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot430.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/430.jpg"/></a> | Buyer - | Bid: - |
| Lot 431 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot431.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/431.jpg"/></a> | Buyer - | Bid: - |
| Lot 432 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot432.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/432.jpg"/></a> | Buyer - | Bid: - |
| Lot 433 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot433.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/433.jpg"/></a> | Buyer - | Bid: - |
| Lot 434 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot434.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/434.jpg"/></a> | Buyer - | Bid: - |
| Lot 435 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot435.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/435.jpg"/></a> | Buyer - | Bid: - |
| Lot 436 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot436.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/436.jpg"/></a> | Buyer - | Bid: - |
| Lot 437 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot437.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/437.jpg"/></a> | Buyer - | Bid: - |
| Lot 438 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot438.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/438.jpg"/></a> | Buyer - | Bid: - |
| Lot 439 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot439.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/439.jpg"/></a> | Buyer - | Bid: - |
| Lot 440 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot440.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/440.jpg"/></a> | Buyer - | Bid: - |
| Lot 441 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot441.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/441.jpg"/></a> | Buyer - | Bid: - |
| Lot 442 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot442.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/442.jpg"/></a> | Buyer - | Bid: - |
| Lot 443 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot443.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/443.jpg"/></a> | Buyer - | Bid: - |
| Lot 444 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot444.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/444.jpg"/></a> | Buyer - | Bid: - |
| Lot 445 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot445.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/445.jpg"/></a> | Buyer - | Bid: - |
| Lot 446 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot446.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/446.jpg"/></a> | Buyer - | Bid: - |
| Lot 447 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot447.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/447.jpg"/></a> | Buyer - | Bid: - |
| Lot 448 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot448.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/448.jpg"/></a> | Buyer - | Bid: - |
| Lot 449 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot449.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/449.jpg"/></a> | Buyer - | Bid: - |
| Lot 450 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot450.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/450.jpg"/></a> | Buyer - | Bid: - |
| Lot 451 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot451.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/451.jpg"/></a> | Buyer - | Bid: - |
| Lot 452 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot452.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/452.jpg"/></a> | Buyer - | Bid: - |
| Lot 453 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot453.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/453.jpg"/></a> | Buyer - | Bid: - |
| Lot 454 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot454.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/454.jpg"/></a> | Buyer - | Bid: - |
| Lot 455 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot455.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/455.jpg"/></a> | Buyer - | Bid: - |
| Lot 456 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot456.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/456.jpg"/></a> | Buyer - | Bid: - |
| Lot 457 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot457.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/457.jpg"/></a> | Buyer - | Bid: - |
| Lot 458 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot458.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/458.jpg"/></a> | Buyer - | Bid: - |
| Lot 459 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot459.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/459.jpg"/></a> | Buyer - | Bid: - |
| Lot 460 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot460.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/460.jpg"/></a> | Buyer - | Bid: - |
| Lot 461 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot461.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/461.jpg"/></a> | Buyer - | Bid: - |
| Lot 462 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot462.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/462.jpg"/></a> | Buyer - | Bid: - |
| Lot 463 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot463.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/463.jpg"/></a> | Buyer - | Bid: - |
| Lot 464 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot464.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/464.jpg"/></a> | Buyer - | Bid: - |
| Lot 465 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot465.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/465.jpg"/></a> | Buyer - | Bid: - |
| Lot 466 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot466.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/466.jpg"/></a> | Buyer - | Bid: - |
| Lot 467 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot467.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/467.jpg"/></a> | Buyer - | Bid: - |
| Lot 468 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot468.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/468.jpg"/></a> | Buyer - | Bid: - |
| Lot 469 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot469.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/469.jpg"/></a> | Buyer - | Bid: - |
| Lot 470 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot470.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/470.jpg"/></a> | Buyer - | Bid: - |
| Lot 471 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot471.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/471.jpg"/></a> | Buyer - | Bid: - |
| Lot 472 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot472.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/472.jpg"/></a> | Buyer - | Bid: - |
| Lot 473 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot473.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/473.jpg"/></a> | Buyer - | Bid: - |
| Lot 474 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot474.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/474.jpg"/></a> | Buyer - | Bid: - |
| Lot 475 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot475.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/475.jpg"/></a> | Buyer - | Bid: - |
| Lot 476 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot476.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/476.jpg"/></a> | Buyer - | Bid: - |
| Lot 477 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot477.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/477.jpg"/></a> | Buyer - | Bid: - |
| Lot 478 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot478.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/478.jpg"/></a> | Buyer - | Bid: - |
| Lot 479 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot479.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/479.jpg"/></a> | Buyer - | Bid: - |
| Lot 480 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot480.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/480.jpg"/></a> | Buyer - | Bid: - |
| Lot 481 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot481.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/481.jpg"/></a> | Buyer - | Bid: - |
| Lot 482 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot482.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/482.jpg"/></a> | Buyer - | Bid: - |
| Lot 483 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot483.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/483.jpg"/></a> | Buyer - | Bid: - |
| Lot 484 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot484.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/484.jpg"/></a> | Buyer - | Bid: - |
| Lot 485 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot485.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/485.jpg"/></a> | Buyer - | Bid: - |
| Lot 486 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot486.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/486.jpg"/></a> | Buyer - | Bid: - |
| Lot 487 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot487.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/487.jpg"/></a> | Buyer - | Bid: - |
| Lot 488 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot488.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/488.jpg"/></a> | Buyer - | Bid: - |
| Lot 489 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot489.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/489.jpg"/></a> | Buyer - | Bid: - |
| Lot 490 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot490.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/490.jpg"/></a> | Buyer - | Bid: - |
| Lot 491 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot491.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/491.jpg"/></a> | Buyer - | Bid: - |
| Lot 492 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot492.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/492.jpg"/></a> | Buyer - | Bid: - |
| Lot 493 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot493.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/493.jpg"/></a> | Buyer - | Bid: - |
| Lot 494 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot494.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/494.jpg"/></a> | Buyer - | Bid: - |
| Lot 495 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot495.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/495.jpg"/></a> | Buyer - | Bid: - |
| Lot 496 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot496.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/496.jpg"/></a> | Buyer - | Bid: - |
| Lot 497 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot497.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/497.jpg"/></a> | Buyer - | Bid: - |
| Lot 498 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot498.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/498.jpg"/></a> | Buyer - | Bid: - |
| Lot 499 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot499.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/499.jpg"/></a> | Buyer - | Bid: - |
| Lot 500 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot500.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/500.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-4.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | **5** | [6](items-6.md) | [Next](items-6.md)
