# Buyer 2's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 155 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot155.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/155.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 221 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot221.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/221.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $1500.00 USD | Highest bid: $1500.00 USD |
| Lot 222 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot222.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/222.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $200.00 USD | Highest bid: $200.00 USD |
| Lot 229 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot229.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/229.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 237 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot237.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/237.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 260 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot260.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/260.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 301 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot301.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/301.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $60.00 USD | Highest bid: $60.00 USD |
| Lot 307 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot307.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/307.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $47.00 USD | Highest bid: $47.00 USD |
| Lot 333 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot333.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/333.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $180.00 USD | Highest bid: $180.00 USD |
| Lot 346 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot346.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/346.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $75.00 USD | Highest bid: $75.00 USD |
| Lot 347 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot347.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/347.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 348 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot348.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/348.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
| Lot 349 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot349.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/349.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $200.00 USD | Highest bid: $200.00 USD |
| Lot 350 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot350.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/350.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $22.00 USD | Highest bid: $22.00 USD |
| Lot 351 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot351.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/351.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $47.00 USD | Highest bid: $47.00 USD |
| Lot 353 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot353.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/353.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 408 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot408.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/408.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $16.00 USD | Highest bid: $16.00 USD |
| Lot 409 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot409.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/409.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $12.00 USD | Highest bid: $12.00 USD |
| Lot 522 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot522.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/522.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 523 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot523.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/523.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
