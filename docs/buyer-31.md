# Buyer 31's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 2 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot2.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/2.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $130.00 USD | Highest bid: $50000.00 USD |
| Lot 3 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot3.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/3.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $220.00 USD | Highest bid: $625.00 USD |
| Lot 28 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot28.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/28.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $300.00 USD | Highest bid: $350.00 USD |
| Lot 133 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot133.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/133.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $40.00 USD |
| Lot 136 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot136.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/136.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $42.00 USD |
| Lot 137 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot137.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/137.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $70.00 USD | Highest bid: $125.00 USD |
| Lot 148 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot148.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/148.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $60.00 USD | Highest bid: $70.00 USD |
| Lot 213 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot213.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/213.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $65.00 USD | Highest bid: $170.00 USD |
| Lot 261 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot261.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/261.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $60.00 USD |
| Lot 299 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot299.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/299.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $25.00 USD |
| Lot 301 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot301.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/301.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $60.00 USD |
| Lot 302 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot302.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/302.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $6.00 USD |
| Lot 406 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot406.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/406.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 425 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot425.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/425.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 429 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot429.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/429.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 431 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot431.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/431.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 461 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot461.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/461.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 518 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot518.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/518.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 527 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot527.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/527.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $50.00 USD |
