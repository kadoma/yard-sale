# Buyer 4's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 3 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot3.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/3.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $400.00 USD | Highest bid: $625.00 USD |
| Lot 218 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot218.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/218.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 227 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot227.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/227.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $60.00 USD |
| Lot 233 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot233.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/233.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 288 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot288.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/288.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 304 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot304.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/304.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $150.00 USD | Highest bid: $200.00 USD |
| Lot 330 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot330.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/330.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 355 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot355.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/355.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 356 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot356.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/356.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 387 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot387.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/387.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 531 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot531.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/531.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $37.00 USD |
