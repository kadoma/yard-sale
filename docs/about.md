# About

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center><font size="5">[**Click here to see check the status of bids where only the lots you have bid on are displayed**](buyers.md)</font></center>

## Telephonic Yard Sale

No 3 Conway Road  
Mornington  
KADOMA

**Book for an appointment to view items for sale by phoning 0773466270.**

Viewing from Monday 13 September, 2021 to Friday 24 September 2021. Viewing times will be from 9am to 4pm.

Due to Covid restrictions - limited to 5 people per hour.

Buying Cards/Numbers are issued with a US$50.00 deposit. The deposit is refundable if no purchases are made.

Bidding will start from 13 September to 24 September. Highest bid @ 4 PM on 24 September will win.

**Method of placing a bid:** Write down your bid price and buying number on the ticket on the item you wish to purchase.

You can check what the highest bid is at the end of each day, in order to secure the item you wish to purchase. If you wish to increase your bid amount, this can be done daily by phone, until 4pm on 24 September.

Purchasers will be notified of their winning bids on Monday 27, September 2021.

Purchases must be paid for in FULL by 4pm on Wednesday 29, September 2021.

There will be a Purchaser's Levy charge of 10% on the final bid price.

If goods are not paid for by 4pm on Wednesday, 29 September, 2021, the item will be sold to the 2nd highest bidder.
