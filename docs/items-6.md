# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here to see check the status of bids where only the lots you have bid on are displayed**](buyers.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

[Prev](items-5.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | **6**

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 501 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot501.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/501.jpg"/></a> | Buyer - | Bid: - |
| Lot 502 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot502.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/502.jpg"/></a> | Buyer - | Bid: - |
| Lot 503 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot503.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/503.jpg"/></a> | Buyer - | Bid: - |
| Lot 504 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot504.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/504.jpg"/></a> | Buyer - | Bid: - |
| Lot 505 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot505.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/505.jpg"/></a> | Buyer - | Bid: - |
| Lot 506 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot506.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/506.jpg"/></a> | Buyer - | Bid: - |
| Lot 507 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot507.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/507.jpg"/></a> | Buyer - | Bid: - |
| Lot 508 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot508.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/508.jpg"/></a> | Buyer - | Bid: - |
| Lot 509 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot509.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/509.jpg"/></a> | Buyer - | Bid: - |
| Lot 510 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot510.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/510.jpg"/></a> | Buyer - | Bid: - |
| Lot 511 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot511.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/511.jpg"/></a> | Buyer - | Bid: - |
| Lot 512 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot512.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/512.jpg"/></a> | Buyer - | Bid: - |
| Lot 513 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot513.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/513.jpg"/></a> | Buyer - | Bid: - |
| Lot 514 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot514.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/514.jpg"/></a> | Buyer - | Bid: - |
| Lot 515 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot515.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/515.jpg"/></a> | Buyer - | Bid: - |
| Lot 516 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot516.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/516.jpg"/></a> | Buyer - | Bid: - |
| Lot 517 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot517.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/517.jpg"/></a> | Buyer - | Bid: - |
| Lot 518 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot518.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/518.jpg"/></a> | Buyer - | Bid: - |
| Lot 519 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot519.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/519.jpg"/></a> | Buyer - | Bid: - |
| Lot 520 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot520.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/520.jpg"/></a> | Buyer - | Bid: - |
| Lot 521 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot521.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/521.jpg"/></a> | Buyer - | Bid: - |
| Lot 522 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot522.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/522.jpg"/></a> | Buyer - | Bid: - |
| Lot 523 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot523.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/523.jpg"/></a> | Buyer - | Bid: - |
| Lot 524 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot524.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/524.jpg"/></a> | Buyer - | Bid: - |
| Lot 525 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot525.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/525.jpg"/></a> | Buyer - | Bid: - |
| Lot 526 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot526.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/526.jpg"/></a> | Buyer - | Bid: - |
| Lot 527 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot527.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/527.jpg"/></a> | Buyer - | Bid: - |
| Lot 528 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot528.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/528.jpg"/></a> | Buyer - | Bid: - |
| Lot 529 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot529.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/529.jpg"/></a> | Buyer - | Bid: - |
| Lot 530 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot530.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/530.jpg"/></a> | Buyer - | Bid: - |
| Lot 531 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot531.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/531.jpg"/></a> | Buyer - | Bid: - |
| Lot 532 | <a href="https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Fmobile%2F000%2F013%2F564%2Fdoge.jpg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/532.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-5.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | **6**
