# Buyer 26's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 32 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot32.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/32.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $45.00 USD |
| Lot 39 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot39.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/39.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $28.00 USD | Highest bid: $30.00 USD |
| Lot 43 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot43.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/43.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $12.00 USD | Highest bid: $20.00 USD |
| Lot 167 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot167.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/167.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $120.00 USD | Highest bid: $140.00 USD |
| Lot 188 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot188.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/188.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $43.00 USD | Highest bid: $175.00 USD |
| Lot 220 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot220.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/220.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $20.00 USD |
| Lot 280 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot280.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/280.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $6.00 USD | Highest bid: $7.00 USD |
| Lot 293 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot293.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/293.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 300 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot300a.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/300.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 363 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot363.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/363.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $6.00 USD | Highest bid: $10.00 USD |
| Lot 395 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot395.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/395.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $16.00 USD | Highest bid: $22.00 USD |
