# Buyer 18's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 5 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot5.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/5.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $2950.00 USD | Highest bid: $2950.00 USD |
| Lot 7 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot7.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/7.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $20.00 USD |
| Lot 35 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot35.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/35.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $45.00 USD | Highest bid: $50.00 USD |
| Lot 126 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot126.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/126.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $60.00 USD | Highest bid: $200.00 USD |
| Lot 131 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot131.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/131.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $12.00 USD | Highest bid: $40.00 USD |
| Lot 162 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot162.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/162.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $320.00 USD | Highest bid: $330.00 USD |
| Lot 165 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot165.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/165.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $60.00 USD |
| Lot 170 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot170.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/170.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $120.00 USD | Highest bid: $170.00 USD |
| Lot 177 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot177.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/177.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $620.00 USD | Highest bid: $1100.00 USD |
| Lot 242 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot242.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/242.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $6.00 USD | Highest bid: $10.00 USD |
| Lot 257 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot257.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/257.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $6.00 USD | Highest bid: $70.00 USD |
| Lot 265 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot265.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/265.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $14.00 USD | Highest bid: $15.00 USD |
| Lot 275 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot275.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/275.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $12.00 USD | Highest bid: $12.00 USD |
| Lot 528 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot528.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/528.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
