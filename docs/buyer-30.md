# Buyer 30's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 8 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot8.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/8.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $90.00 USD | Highest bid: $95.00 USD |
| Lot 9 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot9.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/9.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $39.00 USD |
| Lot 223 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot223.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/223.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $55.00 USD |
| Lot 425 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot425.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/425.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $35.00 USD |
| Lot 465 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot465.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/465.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $50.00 USD |
