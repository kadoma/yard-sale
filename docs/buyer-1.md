# Buyer 1's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 1 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot1.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/1.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $100000.00 USD | Highest bid: $100000.00 USD |
| Lot 169 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot169.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/169.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $60.00 USD | Highest bid: $60.00 USD |
| Lot 218 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot218.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/218.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $35.00 USD |
| Lot 282 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot282.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/282.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $4.00 USD | Highest bid: $4.00 USD |
| Lot 283 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot283.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/283.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $8.00 USD | Highest bid: $8.00 USD |
| Lot 339 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot339.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/339.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $11.00 USD | Highest bid: $11.00 USD |
| Lot 385 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot385.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/385.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 395 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot395.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/395.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $11.00 USD | Highest bid: $22.00 USD |
