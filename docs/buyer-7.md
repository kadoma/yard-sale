# Buyer 7's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 4 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot4.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/4.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $420.00 USD |
| Lot 26 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot26.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/26.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $36.00 USD |
| Lot 37 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot37.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/37.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $210.00 USD | Highest bid: $230.00 USD |
| Lot 50 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot50.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/50.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 109 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot109.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/109.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
| Lot 151 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot151.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/151.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $230.00 USD | Highest bid: $270.00 USD |
| Lot 156 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot156.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/156.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $28.00 USD | Highest bid: $28.00 USD |
| Lot 165 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot165.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/165.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $60.00 USD |
| Lot 231 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot231.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/231.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $30.00 USD |
| Lot 232 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot232.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/232.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 256 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot256.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/256.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $35.00 USD |
| Lot 257 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot257.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/257.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $70.00 USD |
