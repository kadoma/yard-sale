# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here to see check the status of bids where only the lots you have bid on are displayed**](buyers.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

[Prev](index.md) | [1](index.md) | **2** | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [Next](items-3.md)

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 101 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot101.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/101.jpg"/></a> | Buyer - | Bid: - |
| Lot 102 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot102.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/102.jpg"/></a> | Buyer - | Bid: - |
| Lot 103 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot103.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/103.jpg"/></a> | Buyer - | Bid: - |
| Lot 104 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot104.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/104.jpg"/></a> | Buyer - | Bid: - |
| Lot 105 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot105.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/105.jpg"/></a> | Buyer - | Bid: - |
| Lot 106 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot106.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/106.jpg"/></a> | Buyer - | Bid: - |
| Lot 107 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot107.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/107.jpg"/></a> | Buyer - | Bid: - |
| Lot 108 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot108.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/108.jpg"/></a> | Buyer - | Bid: - |
| Lot 109 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot109.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/109.jpg"/></a> | Buyer - | Bid: - |
| Lot 110 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot110.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/110.jpg"/></a> | Buyer - | Bid: - |
| Lot 111 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot111.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/111.jpg"/></a> | Buyer - | Bid: - |
| Lot 112 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot112.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/112.jpg"/></a> | Buyer - | Bid: - |
| Lot 113 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot113.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/113.jpg"/></a> | Buyer - | Bid: - |
| Lot 114 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot114.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/114.jpg"/></a> | Buyer - | Bid: - |
| Lot 115 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot115.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/115.jpg"/></a> | Buyer - | Bid: - |
| Lot 116 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot116.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/116.jpg"/></a> | Buyer - | Bid: - |
| Lot 117 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot117.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/117.jpg"/></a> | Buyer - | Bid: - |
| Lot 118 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot118.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/118.jpg"/></a> | Buyer - | Bid: - |
| Lot 119 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot119.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/119.jpg"/></a> | Buyer - | Bid: - |
| Lot 120 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot120.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/120.jpg"/></a> | Buyer - | Bid: - |
| Lot 121 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot121.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/121.jpg"/></a> | Buyer - | Bid: - |
| Lot 122 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot122.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/122.jpg"/></a> | Buyer - | Bid: - |
| Lot 123 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot123.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/123.jpg"/></a> | Buyer - | Bid: - |
| Lot 124 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot124.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/124.jpg"/></a> | Buyer - | Bid: - |
| Lot 125 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot125.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/125.jpg"/></a> | Buyer - | Bid: - |
| Lot 126 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot126.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/126.jpg"/></a> | Buyer - | Bid: - |
| Lot 127 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot127.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/127.jpg"/></a> | Buyer - | Bid: - |
| Lot 128 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot128.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/128.jpg"/></a> | Buyer - | Bid: - |
| Lot 129 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot129.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/129.jpg"/></a> | Buyer - | Bid: - |
| Lot 130 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot130.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/130.jpg"/></a> | Buyer - | Bid: - |
| Lot 131 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot131.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/131.jpg"/></a> | Buyer - | Bid: - |
| Lot 132 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot132.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/132.jpg"/></a> | Buyer - | Bid: - |
| Lot 133 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot133.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/133.jpg"/></a> | Buyer - | Bid: - |
| Lot 134 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot134.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/134.jpg"/></a> | Buyer - | Bid: - |
| Lot 135 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot135.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/135.jpg"/></a> | Buyer - | Bid: - |
| Lot 136 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot136.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/136.jpg"/></a> | Buyer - | Bid: - |
| Lot 137 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot137.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/137.jpg"/></a> | Buyer - | Bid: - |
| Lot 138 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot138.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/138.jpg"/></a> | Buyer - | Bid: - |
| Lot 139 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot139.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/139.jpg"/></a> | Buyer - | Bid: - |
| Lot 140 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot140.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/140.jpg"/></a> | Buyer - | Bid: - |
| Lot 141 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot141.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/141.jpg"/></a> | Buyer - | Bid: - |
| Lot 142 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot142.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/142.jpg"/></a> | Buyer - | Bid: - |
| Lot 143 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot143.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/143.jpg"/></a> | Buyer - | Bid: - |
| Lot 144 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot144.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/144.jpg"/></a> | Buyer - | Bid: - |
| Lot 145 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot145.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/145.jpg"/></a> | Buyer - | Bid: - |
| Lot 146 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot146.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/146.jpg"/></a> | Buyer - | Bid: - |
| Lot 147 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot147.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/147.jpg"/></a> | Buyer - | Bid: - |
| Lot 148 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot148.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/148.jpg"/></a> | Buyer - | Bid: - |
| Lot 149 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot149.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/149.jpg"/></a> | Buyer - | Bid: - |
| Lot 150 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot150.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/150.jpg"/></a> | Buyer - | Bid: - |
| Lot 151 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot151.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/151.jpg"/></a> | Buyer - | Bid: - |
| Lot 152 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot152.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/152.jpg"/></a> | Buyer - | Bid: - |
| Lot 153 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot153.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/153.jpg"/></a> | Buyer - | Bid: - |
| Lot 154 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot154.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/154.jpg"/></a> | Buyer - | Bid: - |
| Lot 155 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot155.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/155.jpg"/></a> | Buyer - | Bid: - |
| Lot 156 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot156.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/156.jpg"/></a> | Buyer - | Bid: - |
| Lot 157 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot157.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/157.jpg"/></a> | Buyer - | Bid: - |
| Lot 158 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot158.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/158.jpg"/></a> | Buyer - | Bid: - |
| Lot 159 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot159.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/159.jpg"/></a> | Buyer - | Bid: - |
| Lot 160 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot160.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/160.jpg"/></a> | Buyer - | Bid: - |
| Lot 161 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot161.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/161.jpg"/></a> | Buyer - | Bid: - |
| Lot 162 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot162.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/162.jpg"/></a> | Buyer - | Bid: - |
| Lot 163 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot163.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/163.jpg"/></a> | Buyer - | Bid: - |
| Lot 164 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot164.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/164.jpg"/></a> | Buyer - | Bid: - |
| Lot 165 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot165.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/165.jpg"/></a> | Buyer - | Bid: - |
| Lot 166 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot166.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/166.jpg"/></a> | Buyer - | Bid: - |
| Lot 167 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot167.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/167.jpg"/></a> | Buyer - | Bid: - |
| Lot 168 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot168.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/168.jpg"/></a> | Buyer - | Bid: - |
| Lot 169 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot169.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/169.jpg"/></a> | Buyer - | Bid: - |
| Lot 170 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot170.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/170.jpg"/></a> | Buyer - | Bid: - |
| Lot 171 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot171.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/171.jpg"/></a> | Buyer - | Bid: - |
| Lot 172 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot172.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/172.jpg"/></a> | Buyer - | Bid: - |
| Lot 173 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot173.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/173.jpg"/></a> | Buyer - | Bid: - |
| Lot 174 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot174.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/174.jpg"/></a> | Buyer - | Bid: - |
| Lot 175 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot175.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/175.jpg"/></a> | Buyer - | Bid: - |
| Lot 176 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot176.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/176.jpg"/></a> | Buyer - | Bid: - |
| Lot 177 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot177.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/177.jpg"/></a> | Buyer - | Bid: - |
| Lot 178 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot178.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/178.jpg"/></a> | Buyer - | Bid: - |
| Lot 179 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot179.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/179.jpg"/></a> | Buyer - | Bid: - |
| Lot 180 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot180.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/180.jpg"/></a> | Buyer - | Bid: - |
| Lot 181 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot181.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/181.jpg"/></a> | Buyer - | Bid: - |
| Lot 182 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot182.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/182.jpg"/></a> | Buyer - | Bid: - |
| Lot 183 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot183.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/183.jpg"/></a> | Buyer - | Bid: - |
| Lot 184 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot184.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/184.jpg"/></a> | Buyer - | Bid: - |
| Lot 185 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot185.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/185.jpg"/></a> | Buyer - | Bid: - |
| Lot 186 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot186.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/186.jpg"/></a> | Buyer - | Bid: - |
| Lot 187 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot187.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/187.jpg"/></a> | Buyer - | Bid: - |
| Lot 188 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot188.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/188.jpg"/></a> | Buyer - | Bid: - |
| Lot 189 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot189.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/189.jpg"/></a> | Buyer - | Bid: - |
| Lot 190 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot190.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/190.jpg"/></a> | Buyer - | Bid: - |
| Lot 191 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot191.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/191.jpg"/></a> | Buyer - | Bid: - |
| Lot 192 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot192.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/192.jpg"/></a> | Buyer - | Bid: - |
| Lot 193 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot193.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/193.jpg"/></a> | Buyer - | Bid: - |
| Lot 194 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot194.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/194.jpg"/></a> | Buyer - | Bid: - |
| Lot 195 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot195.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/195.jpg"/></a> | Buyer - | Bid: - |
| Lot 196 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot196.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/196.jpg"/></a> | Buyer - | Bid: - |
| Lot 197 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot197.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/197.jpg"/></a> | Buyer - | Bid: - |
| Lot 198 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot198.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/198.jpg"/></a> | Buyer - | Bid: - |
| Lot 199 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot199.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/199.jpg"/></a> | Buyer - | Bid: - |
| Lot 200 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot200.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/200.jpg"/></a> | Buyer - | Bid: - |

[Prev](index.md) | [1](index.md) | **2** | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [Next](items-3.md)
