# Buyer 11's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 4 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot4.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/4.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $360.00 USD | Highest bid: $420.00 USD |
| Lot 5 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot5.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/5.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $2900.00 USD | Highest bid: $2950.00 USD |
| Lot 137 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot137.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/137.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $125.00 USD |
| Lot 215 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot215.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/215.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $26.00 USD | Highest bid: $40.00 USD |
| Lot 263 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot263.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/263.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $22.00 USD | Highest bid: $30.00 USD |
| Lot 264 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot264.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/264.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 265 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot265.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/265.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $13.00 USD | Highest bid: $15.00 USD |
| Lot 290 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot290.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/290.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $41.00 USD | Highest bid: $45.00 USD |
| Lot 297 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot297.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/297.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $32.00 USD | Highest bid: $35.00 USD |
| Lot 334 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot334.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/334.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $105.00 USD | Highest bid: $120.00 USD |
| Lot 338 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot338.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/338.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $145.00 USD | Highest bid: $145.00 USD |
| Lot 383 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot383.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/383.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $18.00 USD | Highest bid: $35.00 USD |
| Lot 395 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot395.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/395.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $22.00 USD |
| Lot 397 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot397.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/397.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $355.00 USD | Highest bid: $380.00 USD |
| Lot 398 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot398.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/398.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $25.00 USD | Highest bid: $25.00 USD |
| Lot 413 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot413.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/413.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
| Lot 414 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot414.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/414.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $60.00 USD | Highest bid: $60.00 USD |
| Lot 417 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot417.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/417.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $110.00 USD |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $80.00 USD |
| Lot 455 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot455.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/455.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $27.00 USD | Highest bid: $70.00 USD |
| Lot 457 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot457.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/457.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $45.00 USD | Highest bid: $75.00 USD |
| Lot 476 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot476.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/476.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $50.00 USD |
| Lot 482 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot482.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/482.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $30.00 USD |
