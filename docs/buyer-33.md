# Buyer 33's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 77 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot77.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/77.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $35.00 USD |
| Lot 133 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot133.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/133.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 147 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot147.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/147.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 164 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot164.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/164.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $80.00 USD | Highest bid: $90.00 USD |
| Lot 167 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot167.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/167.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $140.00 USD | Highest bid: $140.00 USD |
| Lot 169 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot169.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/169.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $6.00 USD | Highest bid: $60.00 USD |
| Lot 191 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot191.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/191.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 219 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot219.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/219.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $80.00 USD | Highest bid: $85.00 USD |
| Lot 223 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot223.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/223.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $55.00 USD |
| Lot 241 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot241.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/241.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 243 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot243.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/243.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 245 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot245.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/245.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $230.00 USD | Highest bid: $281.00 USD |
| Lot 246 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot246.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/246.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 252 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot252.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/252.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 253 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot253.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/253.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $101.00 USD |
| Lot 255 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot255.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/255.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 256 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot256.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/256.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 259 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot259.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/259.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 288 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot288.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/288.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $4.00 USD | Highest bid: $10.00 USD |
| Lot 299 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot299.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/299.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $25.00 USD | Highest bid: $25.00 USD |
| Lot 337 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot337.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/337.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $200.00 USD | Highest bid: $220.00 USD |
| Lot 429 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot429.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/429.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $30.00 USD |
| Lot 455 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot455.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/455.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $55.00 USD | Highest bid: $70.00 USD |
| Lot 458 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot458.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/458.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $40.00 USD |
| Lot 463 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot463.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/463.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 465 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot465.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/465.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
| Lot 473 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot473.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/473.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
