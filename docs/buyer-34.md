# Buyer 34's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 12 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot12.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/12.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $25.00 USD | Highest bid: $25.00 USD |
| Lot 33 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot33.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/33.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $50.00 USD |
| Lot 34 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot34.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/34.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $15.00 USD |
| Lot 38 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot38.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/38.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $105.00 USD | Highest bid: $120.00 USD |
| Lot 73 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot73.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/73.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $32.00 USD | Highest bid: $32.00 USD |
| Lot 136 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot136.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/136.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $42.00 USD | Highest bid: $42.00 USD |
| Lot 148 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot148.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/148.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $65.00 USD | Highest bid: $70.00 USD |
| Lot 186 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot186.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/186.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
| Lot 187 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot187.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/187.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $185.00 USD | Highest bid: $200.00 USD |
| Lot 199 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot199.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/199.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 201 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot201.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/201.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $11.00 USD | Highest bid: $12.00 USD |
| Lot 206 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot206.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/206.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $45.00 USD | Highest bid: $45.00 USD |
| Lot 219 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot219.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/219.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $85.00 USD |
| Lot 252 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot252.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/252.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $40.00 USD |
| Lot 253 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot253.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/253.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $101.00 USD | Highest bid: $101.00 USD |
| Lot 257 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot257.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/257.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $70.00 USD | Highest bid: $70.00 USD |
| Lot 502 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot502.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/502.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 527 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot527.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/527.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $50.00 USD |
