# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here to see check the status of bids where only the lots you have bid on are displayed**](buyers.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

[Prev](items-2.md) | [1](index.md) | [2](items-2.md) | **3** | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [Next](items-4.md)

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 201 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot201.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/201.jpg"/></a> | Buyer - | Bid: - |
| Lot 202 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot202.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/202.jpg"/></a> | Buyer - | Bid: - |
| Lot 203 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot203.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/203.jpg"/></a> | Buyer - | Bid: - |
| Lot 204 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot204.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/204.jpg"/></a> | Buyer - | Bid: - |
| Lot 205 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot205.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/205.jpg"/></a> | Buyer - | Bid: - |
| Lot 206 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot206.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/206.jpg"/></a> | Buyer - | Bid: - |
| Lot 207 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot207.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/207.jpg"/></a> | Buyer - | Bid: - |
| Lot 208 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot208.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/208.jpg"/></a> | Buyer - | Bid: - |
| Lot 209 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot209.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/209.jpg"/></a> | Buyer - | Bid: - |
| Lot 210 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot210.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/210.jpg"/></a> | Buyer - | Bid: - |
| Lot 211 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot211.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/211.jpg"/></a> | Buyer - | Bid: - |
| Lot 212 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot212.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/212.jpg"/></a> | Buyer - | Bid: - |
| Lot 213 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot213.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/213.jpg"/></a> | Buyer - | Bid: - |
| Lot 214 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot214.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/214.jpg"/></a> | Buyer - | Bid: - |
| Lot 215 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot215.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/215.jpg"/></a> | Buyer - | Bid: - |
| Lot 216 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot216.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/216.jpg"/></a> | Buyer - | Bid: - |
| Lot 217 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot217.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/217.jpg"/></a> | Buyer - | Bid: - |
| Lot 218 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot218.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/218.jpg"/></a> | Buyer - | Bid: - |
| Lot 219 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot219.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/219.jpg"/></a> | Buyer - | Bid: - |
| Lot 220 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot220.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/220.jpg"/></a> | Buyer - | Bid: - |
| Lot 221 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot221.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/221.jpg"/></a> | Buyer - | Bid: - |
| Lot 222 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot222.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/222.jpg"/></a> | Buyer - | Bid: - |
| Lot 223 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot223.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/223.jpg"/></a> | Buyer - | Bid: - |
| Lot 224 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot224.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/224.jpg"/></a> | Buyer - | Bid: - |
| Lot 225 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot225.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/225.jpg"/></a> | Buyer - | Bid: - |
| Lot 226 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot226.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/226.jpg"/></a> | Buyer - | Bid: - |
| Lot 227 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot227.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/227.jpg"/></a> | Buyer - | Bid: - |
| Lot 228 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot228.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/228.jpg"/></a> | Buyer - | Bid: - |
| Lot 229 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot229.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/229.jpg"/></a> | Buyer - | Bid: - |
| Lot 230 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot230.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/230.jpg"/></a> | Buyer - | Bid: - |
| Lot 231 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot231.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/231.jpg"/></a> | Buyer - | Bid: - |
| Lot 232 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot232.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/232.jpg"/></a> | Buyer - | Bid: - |
| Lot 233 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot233.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/233.jpg"/></a> | Buyer - | Bid: - |
| Lot 234 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot234.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/234.jpg"/></a> | Buyer - | Bid: - |
| Lot 235 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot235.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/235.jpg"/></a> | Buyer - | Bid: - |
| Lot 236 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot236.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/236.jpg"/></a> | Buyer - | Bid: - |
| Lot 237 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot237.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/237.jpg"/></a> | Buyer - | Bid: - |
| Lot 238 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot238.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/238.jpg"/></a> | Buyer - | Bid: - |
| Lot 239 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot239.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/239.jpg"/></a> | Buyer - | Bid: - |
| Lot 240 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot240.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/240.jpg"/></a> | Buyer - | Bid: - |
| Lot 241 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot241.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/241.jpg"/></a> | Buyer - | Bid: - |
| Lot 242 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot242.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/242.jpg"/></a> | Buyer - | Bid: - |
| Lot 243 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot243.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/243.jpg"/></a> | Buyer - | Bid: - |
| Lot 244 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot244.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/244.jpg"/></a> | Buyer - | Bid: - |
| Lot 245 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot245.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/245.jpg"/></a> | Buyer - | Bid: - |
| Lot 246 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot246.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/246.jpg"/></a> | Buyer - | Bid: - |
| Lot 247 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot247.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/247.jpg"/></a> | Buyer - | Bid: - |
| Lot 248 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot248.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/248.jpg"/></a> | Buyer - | Bid: - |
| Lot 249 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot249.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/249.jpg"/></a> | Buyer - | Bid: - |
| Lot 250 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot250.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/250.jpg"/></a> | Buyer - | Bid: - |
| Lot 251 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot251.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/251.jpg"/></a> | Buyer - | Bid: - |
| Lot 252 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot252.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/252.jpg"/></a> | Buyer - | Bid: - |
| Lot 253 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot253.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/253.jpg"/></a> | Buyer - | Bid: - |
| Lot 254 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot254.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/254.jpg"/></a> | Buyer - | Bid: - |
| Lot 255 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot255.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/255.jpg"/></a> | Buyer - | Bid: - |
| Lot 256 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot256.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/256.jpg"/></a> | Buyer - | Bid: - |
| Lot 257 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot257.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/257.jpg"/></a> | Buyer - | Bid: - |
| Lot 258 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot258.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/258.jpg"/></a> | Buyer - | Bid: - |
| Lot 259 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot259.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/259.jpg"/></a> | Buyer - | Bid: - |
| Lot 260 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot260.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/260.jpg"/></a> | Buyer - | Bid: - |
| Lot 261 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot261.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/261.jpg"/></a> | Buyer - | Bid: - |
| Lot 262 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot262.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/262.jpg"/></a> | Buyer - | Bid: - |
| Lot 263 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot263.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/263.jpg"/></a> | Buyer - | Bid: - |
| Lot 264 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot264.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/264.jpg"/></a> | Buyer - | Bid: - |
| Lot 265 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot265.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/265.jpg"/></a> | Buyer - | Bid: - |
| Lot 266 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot266.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/266.jpg"/></a> | Buyer - | Bid: - |
| Lot 267 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot267.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/267.jpg"/></a> | Buyer - | Bid: - |
| Lot 268 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot268.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/268.jpg"/></a> | Buyer - | Bid: - |
| Lot 269 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot269.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/269.jpg"/></a> | Buyer - | Bid: - |
| Lot 270 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot270.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/270.jpg"/></a> | Buyer - | Bid: - |
| Lot 271 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot271.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/271.jpg"/></a> | Buyer - | Bid: - |
| Lot 272 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot272.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/272.jpg"/></a> | Buyer - | Bid: - |
| Lot 273 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot273.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/273.jpg"/></a> | Buyer - | Bid: - |
| Lot 274 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot274.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/274.jpg"/></a> | Buyer - | Bid: - |
| Lot 275 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot275.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/275.jpg"/></a> | Buyer - | Bid: - |
| Lot 276 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot276.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/276.jpg"/></a> | Buyer - | Bid: - |
| Lot 277 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot277.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/277.jpg"/></a> | Buyer - | Bid: - |
| Lot 278 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot278.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/278.jpg"/></a> | Buyer - | Bid: - |
| Lot 279 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot279.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/279.jpg"/></a> | Buyer - | Bid: - |
| Lot 280 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot280.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/280.jpg"/></a> | Buyer - | Bid: - |
| Lot 281 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot281.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/281.jpg"/></a> | Buyer - | Bid: - |
| Lot 282 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot282.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/282.jpg"/></a> | Buyer - | Bid: - |
| Lot 283 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot283.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/283.jpg"/></a> | Buyer - | Bid: - |
| Lot 284 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot284.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/284.jpg"/></a> | Buyer - | Bid: - |
| Lot 285 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot285.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/285.jpg"/></a> | Buyer - | Bid: - |
| Lot 286 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot286.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/286.jpg"/></a> | Buyer - | Bid: - |
| Lot 287 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot287.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/287.jpg"/></a> | Buyer - | Bid: - |
| Lot 288 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot288.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/288.jpg"/></a> | Buyer - | Bid: - |
| Lot 289 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot289.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/289.jpg"/></a> | Buyer - | Bid: - |
| Lot 290 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot290.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/290.jpg"/></a> | Buyer - | Bid: - |
| Lot 291 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot291.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/291.jpg"/></a> | Buyer - | Bid: - |
| Lot 292 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot292.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/292.jpg"/></a> | Buyer - | Bid: - |
| Lot 293 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot293.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/293.jpg"/></a> | Buyer - | Bid: - |
| Lot 294 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot294.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/294.jpg"/></a> | Buyer - | Bid: - |
| Lot 295 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot295.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/295.jpg"/></a> | Buyer - | Bid: - |
| Lot 296 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot296.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/296.jpg"/></a> | Buyer - | Bid: - |
| Lot 297 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot297.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/297.jpg"/></a> | Buyer - | Bid: - |
| Lot 298 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot298.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/298.jpg"/></a> | Buyer - | Bid: - |
| Lot 299 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot299.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/299.jpg"/></a> | Buyer - | Bid: - |
| Lot 300 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot300a.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/300.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-2.md) | [1](index.md) | [2](items-2.md) | **3** | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [Next](items-4.md)
