# Buyer 44's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 2 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot2.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/2.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $145.00 USD | Highest bid: $50000.00 USD |
| Lot 8 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot8.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/8.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $90.00 USD | Highest bid: $95.00 USD |
| Lot 40 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot40.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/40.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $60.00 USD | Highest bid: $70.00 USD |
| Lot 53 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot53.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/53.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $85.00 USD | Highest bid: $100.00 USD |
| Lot 131 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot131.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/131.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $32.00 USD | Highest bid: $40.00 USD |
| Lot 149 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot149.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/149.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $27.00 USD | Highest bid: $30.00 USD |
| Lot 159 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot159.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/159.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $400.00 USD | Highest bid: $410.00 USD |
| Lot 168 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot168.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/168.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $555.00 USD | Highest bid: $600.00 USD |
| Lot 176 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot176.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/176.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $590.00 USD | Highest bid: $620.00 USD |
| Lot 182 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot182.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/182.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $2900.00 USD | Highest bid: $2910.00 USD |
| Lot 187 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot187.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/187.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $180.00 USD | Highest bid: $200.00 USD |
| Lot 188 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot188.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/188.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $170.00 USD | Highest bid: $175.00 USD |
| Lot 245 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot245.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/245.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $271.00 USD | Highest bid: $281.00 USD |
| Lot 251 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot251.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/251.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $210.00 USD | Highest bid: $220.00 USD |
| Lot 298 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot298.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/298.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $30.00 USD |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $80.00 USD |
| Lot 476 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot476.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/476.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $43.00 USD | Highest bid: $50.00 USD |
