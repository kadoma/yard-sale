# Buyer 39's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 27 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot27.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/27.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
| Lot 34 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot34.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/34.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
| Lot 35 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot35.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/35.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $42.00 USD | Highest bid: $50.00 USD |
| Lot 74 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot74.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/74.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $60.00 USD | Highest bid: $60.00 USD |
| Lot 87 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot87.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/87.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $85.00 USD | Highest bid: $100.00 USD |
| Lot 89 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot89.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/89.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 92 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot92.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/92.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $55.00 USD | Highest bid: $60.00 USD |
| Lot 100 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot100.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/100.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $36.00 USD |
| Lot 108 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot108.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/108.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $112.00 USD | Highest bid: $190.00 USD |
| Lot 126 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot126.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/126.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $196.00 USD | Highest bid: $200.00 USD |
| Lot 131 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot131.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/131.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 138 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot138.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/138.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $135.00 USD | Highest bid: $200.00 USD |
| Lot 168 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot168.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/168.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $510.00 USD | Highest bid: $600.00 USD |
| Lot 215 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot215.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/215.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $38.00 USD | Highest bid: $40.00 USD |
| Lot 265 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot265.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/265.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
| Lot 335 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot335.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/335.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $92.00 USD | Highest bid: $92.00 USD |
| Lot 476 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot476.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/476.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $36.00 USD | Highest bid: $50.00 USD |
