# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here to see check the status of bids where only the lots you have bid on are displayed**](buyers.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

[Prev](items-3.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | **4** | [5](items-5.md) | [6](items-6.md) | [Next](items-5.md)

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 301 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot301.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/301.jpg"/></a> | Buyer - | Bid: - |
| Lot 302 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot302.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/302.jpg"/></a> | Buyer - | Bid: - |
| Lot 303 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot303.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/303.jpg"/></a> | Buyer - | Bid: - |
| Lot 304 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot304.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/304.jpg"/></a> | Buyer - | Bid: - |
| Lot 305 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot305.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/305.jpg"/></a> | Buyer - | Bid: - |
| Lot 306 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot306.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/306.jpg"/></a> | Buyer - | Bid: - |
| Lot 307 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot307.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/307.jpg"/></a> | Buyer - | Bid: - |
| Lot 308 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot308.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/308.jpg"/></a> | Buyer - | Bid: - |
| Lot 309 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot309.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/309.jpg"/></a> | Buyer - | Bid: - |
| Lot 310 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot310.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/310.jpg"/></a> | Buyer - | Bid: - |
| Lot 311 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot311.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/311.jpg"/></a> | Buyer - | Bid: - |
| Lot 312 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot312.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/312.jpg"/></a> | Buyer - | Bid: - |
| Lot 313 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot313.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/313.jpg"/></a> | Buyer - | Bid: - |
| Lot 314 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot314.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/314.jpg"/></a> | Buyer - | Bid: - |
| Lot 315 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot315.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/315.jpg"/></a> | Buyer - | Bid: - |
| Lot 316 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot316.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/316.jpg"/></a> | Buyer - | Bid: - |
| Lot 317 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot317.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/317.jpg"/></a> | Buyer - | Bid: - |
| Lot 318 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot318.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/318.jpg"/></a> | Buyer - | Bid: - |
| Lot 319 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot319.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/319.jpg"/></a> | Buyer - | Bid: - |
| Lot 320 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot320.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/320.jpg"/></a> | Buyer - | Bid: - |
| Lot 321 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot321.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/321.jpg"/></a> | Buyer - | Bid: - |
| Lot 322 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot322.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/322.jpg"/></a> | Buyer - | Bid: - |
| Lot 323 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot323.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/323.jpg"/></a> | Buyer - | Bid: - |
| Lot 324 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot324.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/324.jpg"/></a> | Buyer - | Bid: - |
| Lot 325 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot325.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/325.jpg"/></a> | Buyer - | Bid: - |
| Lot 326 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot326.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/326.jpg"/></a> | Buyer - | Bid: - |
| Lot 327 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot327.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/327.jpg"/></a> | Buyer - | Bid: - |
| Lot 328 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot328.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/328.jpg"/></a> | Buyer - | Bid: - |
| Lot 329 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot329.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/329.jpg"/></a> | Buyer - | Bid: - |
| Lot 330 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot330.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/330.jpg"/></a> | Buyer - | Bid: - |
| Lot 331 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot331.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/331.jpg"/></a> | Buyer - | Bid: - |
| Lot 332 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot332.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/332.jpg"/></a> | Buyer - | Bid: - |
| Lot 333 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot333.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/333.jpg"/></a> | Buyer - | Bid: - |
| Lot 334 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot334.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/334.jpg"/></a> | Buyer - | Bid: - |
| Lot 335 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot335.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/335.jpg"/></a> | Buyer - | Bid: - |
| Lot 336 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot336.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/336.jpg"/></a> | Buyer - | Bid: - |
| Lot 337 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot337.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/337.jpg"/></a> | Buyer - | Bid: - |
| Lot 338 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot338.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/338.jpg"/></a> | Buyer - | Bid: - |
| Lot 339 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot339.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/339.jpg"/></a> | Buyer - | Bid: - |
| Lot 340 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot340.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/340.jpg"/></a> | Buyer - | Bid: - |
| Lot 341 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot341.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/341.jpg"/></a> | Buyer - | Bid: - |
| Lot 342 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot342.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/342.jpg"/></a> | Buyer - | Bid: - |
| Lot 343 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot343.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/343.jpg"/></a> | Buyer - | Bid: - |
| Lot 344 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot344.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/344.jpg"/></a> | Buyer - | Bid: - |
| Lot 345 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot345.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/345.jpg"/></a> | Buyer - | Bid: - |
| Lot 346 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot346.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/346.jpg"/></a> | Buyer - | Bid: - |
| Lot 347 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot347.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/347.jpg"/></a> | Buyer - | Bid: - |
| Lot 348 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot348.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/348.jpg"/></a> | Buyer - | Bid: - |
| Lot 349 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot349.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/349.jpg"/></a> | Buyer - | Bid: - |
| Lot 350 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot350.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/350.jpg"/></a> | Buyer - | Bid: - |
| Lot 351 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot351.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/351.jpg"/></a> | Buyer - | Bid: - |
| Lot 352 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot352.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/352.jpg"/></a> | Buyer - | Bid: - |
| Lot 353 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot353.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/353.jpg"/></a> | Buyer - | Bid: - |
| Lot 354 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot354.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/354.jpg"/></a> | Buyer - | Bid: - |
| Lot 355 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot355.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/355.jpg"/></a> | Buyer - | Bid: - |
| Lot 356 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot356.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/356.jpg"/></a> | Buyer - | Bid: - |
| Lot 357 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot357.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/357.jpg"/></a> | Buyer - | Bid: - |
| Lot 358 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot358.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/358.jpg"/></a> | Buyer - | Bid: - |
| Lot 359 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot359.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/359.jpg"/></a> | Buyer - | Bid: - |
| Lot 360 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot360.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/360.jpg"/></a> | Buyer - | Bid: - |
| Lot 361 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot361.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/361.jpg"/></a> | Buyer - | Bid: - |
| Lot 362 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot362.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/362.jpg"/></a> | Buyer - | Bid: - |
| Lot 363 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot363.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/363.jpg"/></a> | Buyer - | Bid: - |
| Lot 364 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot364.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/364.jpg"/></a> | Buyer - | Bid: - |
| Lot 365 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot365.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/365.jpg"/></a> | Buyer - | Bid: - |
| Lot 366 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot366.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/366.jpg"/></a> | Buyer - | Bid: - |
| Lot 367 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot367.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/367.jpg"/></a> | Buyer - | Bid: - |
| Lot 368 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot368.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/368.jpg"/></a> | Buyer - | Bid: - |
| Lot 369 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot369.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/369.jpg"/></a> | Buyer - | Bid: - |
| Lot 370 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot370.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/370.jpg"/></a> | Buyer - | Bid: - |
| Lot 371 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot371.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/371.jpg"/></a> | Buyer - | Bid: - |
| Lot 372 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot372.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/372.jpg"/></a> | Buyer - | Bid: - |
| Lot 373 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot373.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/373.jpg"/></a> | Buyer - | Bid: - |
| Lot 374 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot374.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/374.jpg"/></a> | Buyer - | Bid: - |
| Lot 375 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot375.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/375.jpg"/></a> | Buyer - | Bid: - |
| Lot 376 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot376.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/376.jpg"/></a> | Buyer - | Bid: - |
| Lot 377 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot377.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/377.jpg"/></a> | Buyer - | Bid: - |
| Lot 378 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot378.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/378.jpg"/></a> | Buyer - | Bid: - |
| Lot 379 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot379.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/379.jpg"/></a> | Buyer - | Bid: - |
| Lot 380 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot380.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/380.jpg"/></a> | Buyer - | Bid: - |
| Lot 381 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot381.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/381.jpg"/></a> | Buyer - | Bid: - |
| Lot 382 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot382.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/382.jpg"/></a> | Buyer - | Bid: - |
| Lot 383 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot383.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/383.jpg"/></a> | Buyer - | Bid: - |
| Lot 384 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot384.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/384.jpg"/></a> | Buyer - | Bid: - |
| Lot 385 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot385.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/385.jpg"/></a> | Buyer - | Bid: - |
| Lot 386 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot386.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/386.jpg"/></a> | Buyer - | Bid: - |
| Lot 387 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot387.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/387.jpg"/></a> | Buyer - | Bid: - |
| Lot 388 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot388.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/388.jpg"/></a> | Buyer - | Bid: - |
| Lot 389 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot389.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/389.jpg"/></a> | Buyer - | Bid: - |
| Lot 390 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot390.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/390.jpg"/></a> | Buyer - | Bid: - |
| Lot 391 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot391.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/391.jpg"/></a> | Buyer - | Bid: - |
| Lot 392 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot392.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/392.jpg"/></a> | Buyer - | Bid: - |
| Lot 393 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot393.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/393.jpg"/></a> | Buyer - | Bid: - |
| Lot 394 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot394.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/394.jpg"/></a> | Buyer - | Bid: - |
| Lot 395 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot395.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/395.jpg"/></a> | Buyer - | Bid: - |
| Lot 396 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot396.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/396.jpg"/></a> | Buyer - | Bid: - |
| Lot 397 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot397.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/397.jpg"/></a> | Buyer - | Bid: - |
| Lot 398 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot398.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/398.jpg"/></a> | Buyer - | Bid: - |
| Lot 399 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot399.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/399.jpg"/></a> | Buyer - | Bid: - |
| Lot 400 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot400.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/400.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-3.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | **4** | [5](items-5.md) | [6](items-6.md) | [Next](items-5.md)
