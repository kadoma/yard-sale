# Buyer 43's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 73 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot73.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/73.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $32.00 USD |
| Lot 131 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot131.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/131.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $40.00 USD |
| Lot 138 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot138.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/138.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $150.00 USD | Highest bid: $200.00 USD |
| Lot 144 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot144.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/144.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $30.00 USD |
| Lot 249 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot249.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/249.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 472 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot472.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/472.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
