# Buyer 38's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 107 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot107.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/107.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 117 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot117.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/117.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $101.00 USD |
| Lot 132 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot132.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/132.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $40.00 USD |
| Lot 148 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot148.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/148.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $75.00 USD | Highest bid: $75.00 USD |
| Lot 176 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot176.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/176.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $600.00 USD | Highest bid: $620.00 USD |
| Lot 177 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot177.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/177.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $1000.00 USD | Highest bid: $1100.00 USD |
| Lot 178 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot178.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/178.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $180.00 USD | Highest bid: $190.00 USD |
| Lot 222 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot222.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/222.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $180.00 USD | Highest bid: $200.00 USD |
| Lot 251 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot251.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/251.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $180.00 USD | Highest bid: $220.00 USD |
| Lot 298 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot298.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/298.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $35.00 USD |
| Lot 321 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot321.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/321.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $35.00 USD |
| Lot 322 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot322.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/322.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $35.00 USD |
| Lot 323 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot323.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/323.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $35.00 USD |
| Lot 324 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot324.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/324.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $40.00 USD |
| Lot 335 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot335.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/335.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $90.00 USD | Highest bid: $92.00 USD |
| Lot 337 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot337.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/337.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $250.00 USD | Highest bid: $250.00 USD |
| Lot 358 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot358.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/358.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 359 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot359.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/359.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $65.00 USD |
| Lot 385 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot385.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/385.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $30.00 USD |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $45.00 USD | Highest bid: $80.00 USD |
| Lot 455 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot455.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/455.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $65.00 USD | Highest bid: $70.00 USD |
| Lot 456 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot456.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/456.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 457 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot457.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/457.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $60.00 USD | Highest bid: $75.00 USD |
| Lot 465 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot465.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/465.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $70.00 USD | Highest bid: $70.00 USD |
| Lot 476 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot476.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/476.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
| Lot 483 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot483.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/483.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 485 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot485.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/485.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 490 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot490.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/490.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 491 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot491.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/491.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $75.00 USD | Highest bid: $75.00 USD |
| Lot 495 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot495.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/495.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
