# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 901 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/901.jpg"/></a> | Buyer - | Bid: - |
| Lot 902 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/902.jpg"/></a> | Buyer - | Bid: - |
| Lot 903 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/903.jpg"/></a> | Buyer - | Bid: - |
| Lot 904 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/904.jpg"/></a> | Buyer - | Bid: - |
| Lot 905 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/905.jpg"/></a> | Buyer - | Bid: - |
| Lot 906 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/906.jpg"/></a> | Buyer - | Bid: - |
| Lot 907 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/907.jpg"/></a> | Buyer - | Bid: - |
| Lot 908 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/908.jpg"/></a> | Buyer - | Bid: - |
| Lot 909 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/909.jpg"/></a> | Buyer - | Bid: - |
| Lot 910 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/910.jpg"/></a> | Buyer - | Bid: - |
| Lot 911 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/911.jpg"/></a> | Buyer - | Bid: - |
| Lot 912 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/912.jpg"/></a> | Buyer - | Bid: - |
| Lot 913 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/913.jpg"/></a> | Buyer - | Bid: - |
| Lot 914 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/914.jpg"/></a> | Buyer - | Bid: - |
| Lot 915 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/915.jpg"/></a> | Buyer - | Bid: - |
| Lot 916 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/916.jpg"/></a> | Buyer - | Bid: - |
| Lot 917 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/917.jpg"/></a> | Buyer - | Bid: - |
| Lot 918 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/918.jpg"/></a> | Buyer - | Bid: - |
| Lot 919 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/919.jpg"/></a> | Buyer - | Bid: - |
| Lot 920 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/920.jpg"/></a> | Buyer - | Bid: - |
| Lot 921 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/921.jpg"/></a> | Buyer - | Bid: - |
| Lot 922 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/922.jpg"/></a> | Buyer - | Bid: - |
| Lot 923 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/923.jpg"/></a> | Buyer - | Bid: - |
| Lot 924 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/924.jpg"/></a> | Buyer - | Bid: - |
| Lot 925 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/925.jpg"/></a> | Buyer - | Bid: - |
| Lot 926 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/926.jpg"/></a> | Buyer - | Bid: - |
| Lot 927 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/927.jpg"/></a> | Buyer - | Bid: - |
| Lot 928 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/928.jpg"/></a> | Buyer - | Bid: - |
| Lot 929 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/929.jpg"/></a> | Buyer - | Bid: - |
| Lot 930 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/930.jpg"/></a> | Buyer - | Bid: - |
| Lot 931 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/931.jpg"/></a> | Buyer - | Bid: - |
| Lot 932 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/932.jpg"/></a> | Buyer - | Bid: - |
| Lot 933 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/933.jpg"/></a> | Buyer - | Bid: - |
| Lot 934 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/934.jpg"/></a> | Buyer - | Bid: - |
| Lot 935 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/935.jpg"/></a> | Buyer - | Bid: - |
| Lot 936 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/936.jpg"/></a> | Buyer - | Bid: - |
| Lot 937 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/937.jpg"/></a> | Buyer - | Bid: - |
| Lot 938 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/938.jpg"/></a> | Buyer - | Bid: - |
| Lot 939 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/939.jpg"/></a> | Buyer - | Bid: - |
| Lot 940 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/940.jpg"/></a> | Buyer - | Bid: - |
| Lot 941 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/941.jpg"/></a> | Buyer - | Bid: - |
| Lot 942 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/942.jpg"/></a> | Buyer - | Bid: - |
| Lot 943 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/943.jpg"/></a> | Buyer - | Bid: - |
| Lot 944 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/944.jpg"/></a> | Buyer - | Bid: - |
| Lot 945 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/945.jpg"/></a> | Buyer - | Bid: - |
| Lot 946 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/946.jpg"/></a> | Buyer - | Bid: - |
| Lot 947 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/947.jpg"/></a> | Buyer - | Bid: - |
| Lot 948 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/948.jpg"/></a> | Buyer - | Bid: - |
| Lot 949 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/949.jpg"/></a> | Buyer - | Bid: - |
| Lot 950 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/950.jpg"/></a> | Buyer - | Bid: - |
| Lot 951 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/951.jpg"/></a> | Buyer - | Bid: - |
| Lot 952 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/952.jpg"/></a> | Buyer - | Bid: - |
| Lot 953 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/953.jpg"/></a> | Buyer - | Bid: - |
| Lot 954 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/954.jpg"/></a> | Buyer - | Bid: - |
| Lot 955 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/955.jpg"/></a> | Buyer - | Bid: - |
| Lot 956 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/956.jpg"/></a> | Buyer - | Bid: - |
| Lot 957 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/957.jpg"/></a> | Buyer - | Bid: - |
| Lot 958 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/958.jpg"/></a> | Buyer - | Bid: - |
| Lot 959 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/959.jpg"/></a> | Buyer - | Bid: - |
| Lot 960 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/960.jpg"/></a> | Buyer - | Bid: - |
| Lot 961 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/961.jpg"/></a> | Buyer - | Bid: - |
| Lot 962 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/962.jpg"/></a> | Buyer - | Bid: - |
| Lot 963 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/963.jpg"/></a> | Buyer - | Bid: - |
| Lot 964 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/964.jpg"/></a> | Buyer - | Bid: - |
| Lot 965 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/965.jpg"/></a> | Buyer - | Bid: - |
| Lot 966 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/966.jpg"/></a> | Buyer - | Bid: - |
| Lot 967 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/967.jpg"/></a> | Buyer - | Bid: - |
| Lot 968 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/968.jpg"/></a> | Buyer - | Bid: - |
| Lot 969 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/969.jpg"/></a> | Buyer - | Bid: - |
| Lot 970 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/970.jpg"/></a> | Buyer - | Bid: - |
| Lot 971 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/971.jpg"/></a> | Buyer - | Bid: - |
| Lot 972 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/972.jpg"/></a> | Buyer - | Bid: - |
| Lot 973 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/973.jpg"/></a> | Buyer - | Bid: - |
| Lot 974 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/974.jpg"/></a> | Buyer - | Bid: - |
| Lot 975 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/975.jpg"/></a> | Buyer - | Bid: - |
| Lot 976 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/976.jpg"/></a> | Buyer - | Bid: - |
| Lot 977 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/977.jpg"/></a> | Buyer - | Bid: - |
| Lot 978 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/978.jpg"/></a> | Buyer - | Bid: - |
| Lot 979 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/979.jpg"/></a> | Buyer - | Bid: - |
| Lot 980 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/980.jpg"/></a> | Buyer - | Bid: - |
| Lot 981 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/981.jpg"/></a> | Buyer - | Bid: - |
| Lot 982 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/982.jpg"/></a> | Buyer - | Bid: - |
| Lot 983 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/983.jpg"/></a> | Buyer - | Bid: - |
| Lot 984 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/984.jpg"/></a> | Buyer - | Bid: - |
| Lot 985 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/985.jpg"/></a> | Buyer - | Bid: - |
| Lot 986 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/986.jpg"/></a> | Buyer - | Bid: - |
| Lot 987 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/987.jpg"/></a> | Buyer - | Bid: - |
| Lot 988 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/988.jpg"/></a> | Buyer - | Bid: - |
| Lot 989 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/989.jpg"/></a> | Buyer - | Bid: - |
| Lot 990 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/990.jpg"/></a> | Buyer - | Bid: - |
| Lot 991 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/991.jpg"/></a> | Buyer - | Bid: - |
| Lot 992 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/992.jpg"/></a> | Buyer - | Bid: - |
| Lot 993 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/993.jpg"/></a> | Buyer - | Bid: - |
| Lot 994 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/994.jpg"/></a> | Buyer - | Bid: - |
| Lot 995 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/995.jpg"/></a> | Buyer - | Bid: - |
| Lot 996 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/996.jpg"/></a> | Buyer - | Bid: - |
| Lot 997 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/997.jpg"/></a> | Buyer - | Bid: - |
| Lot 998 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/998.jpg"/></a> | Buyer - | Bid: - |
| Lot 999 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/999.jpg"/></a> | Buyer - | Bid: - |
| Lot 1000 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/1000.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-9.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [7](items-7.md) | [8](items-8.md) | [9](items-9.md) | **10**
