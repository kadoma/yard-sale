# Buyer 42's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 92 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot92.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/92.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $60.00 USD | Highest bid: $60.00 USD |
| Lot 105 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot105.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/105.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 142 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot142.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/142.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $21.00 USD |
| Lot 361 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot361.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/361.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $14.00 USD | Highest bid: $14.00 USD |
| Lot 411 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot411.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/411.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 465 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot465.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/465.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $26.00 USD | Highest bid: $50.00 USD |
| Lot 492 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot492.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/492.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $12.00 USD | Highest bid: $12.00 USD |
