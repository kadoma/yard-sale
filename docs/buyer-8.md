# Buyer 8's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 30 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot30.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/30.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $100.00 USD |
| Lot 52 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot52.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/52.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $150.00 USD | Highest bid: $335.00 USD |
| Lot 73 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot73.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/73.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $32.00 USD |
| Lot 137 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot137.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/137.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $125.00 USD |
| Lot 193 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot193.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/193.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $26.00 USD |
| Lot 203 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot203.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/203.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $45.00 USD |
| Lot 206 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot206.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/206.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $45.00 USD |
| Lot 222 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot222.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/222.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $200.00 USD |
| Lot 247 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot247.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/247.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $45.00 USD |
| Lot 257 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot257.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/257.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $70.00 USD |
| Lot 269 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot269.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/269.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $35.00 USD |
| Lot 338 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot338.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/338.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $130.00 USD | Highest bid: $145.00 USD |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $8.00 USD | Highest bid: $80.00 USD |
| Lot 424 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot424.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/424.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 504 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot504.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/504.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $30.00 USD |
| Lot 513 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot513.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/513.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
