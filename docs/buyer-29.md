# Buyer 29's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 38 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot38.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/38.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $60.00 USD | Highest bid: $120.00 USD |
| Lot 137 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot137.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/137.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $80.00 USD | Highest bid: $125.00 USD |
| Lot 146 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot146.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/146.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $45.00 USD | Highest bid: $50.00 USD |
| Lot 187 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot187.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/187.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $120.00 USD | Highest bid: $200.00 USD |
| Lot 212 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot212.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/212.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $50.00 USD | Highest bid: $50.00 USD |
| Lot 213 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot213.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/213.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $160.00 USD | Highest bid: $170.00 USD |
| Lot 259 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot259.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/259.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 261 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot261.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/261.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $45.00 USD | Highest bid: $60.00 USD |
| Lot 333 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot333.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/333.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $165.00 USD | Highest bid: $180.00 USD |
| Lot 337 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot337.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/337.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $120.00 USD | Highest bid: $220.00 USD |
| Lot 359 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot359.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/359.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $65.00 USD |
