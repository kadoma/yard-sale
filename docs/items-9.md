# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 801 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/801.jpg"/></a> | Buyer - | Bid: - |
| Lot 802 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/802.jpg"/></a> | Buyer - | Bid: - |
| Lot 803 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/803.jpg"/></a> | Buyer - | Bid: - |
| Lot 804 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/804.jpg"/></a> | Buyer - | Bid: - |
| Lot 805 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/805.jpg"/></a> | Buyer - | Bid: - |
| Lot 806 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/806.jpg"/></a> | Buyer - | Bid: - |
| Lot 807 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/807.jpg"/></a> | Buyer - | Bid: - |
| Lot 808 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/808.jpg"/></a> | Buyer - | Bid: - |
| Lot 809 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/809.jpg"/></a> | Buyer - | Bid: - |
| Lot 810 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/810.jpg"/></a> | Buyer - | Bid: - |
| Lot 811 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/811.jpg"/></a> | Buyer - | Bid: - |
| Lot 812 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/812.jpg"/></a> | Buyer - | Bid: - |
| Lot 813 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/813.jpg"/></a> | Buyer - | Bid: - |
| Lot 814 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/814.jpg"/></a> | Buyer - | Bid: - |
| Lot 815 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/815.jpg"/></a> | Buyer - | Bid: - |
| Lot 816 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/816.jpg"/></a> | Buyer - | Bid: - |
| Lot 817 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/817.jpg"/></a> | Buyer - | Bid: - |
| Lot 818 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/818.jpg"/></a> | Buyer - | Bid: - |
| Lot 819 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/819.jpg"/></a> | Buyer - | Bid: - |
| Lot 820 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/820.jpg"/></a> | Buyer - | Bid: - |
| Lot 821 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/821.jpg"/></a> | Buyer - | Bid: - |
| Lot 822 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/822.jpg"/></a> | Buyer - | Bid: - |
| Lot 823 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/823.jpg"/></a> | Buyer - | Bid: - |
| Lot 824 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/824.jpg"/></a> | Buyer - | Bid: - |
| Lot 825 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/825.jpg"/></a> | Buyer - | Bid: - |
| Lot 826 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/826.jpg"/></a> | Buyer - | Bid: - |
| Lot 827 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/827.jpg"/></a> | Buyer - | Bid: - |
| Lot 828 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/828.jpg"/></a> | Buyer - | Bid: - |
| Lot 829 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/829.jpg"/></a> | Buyer - | Bid: - |
| Lot 830 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/830.jpg"/></a> | Buyer - | Bid: - |
| Lot 831 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/831.jpg"/></a> | Buyer - | Bid: - |
| Lot 832 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/832.jpg"/></a> | Buyer - | Bid: - |
| Lot 833 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/833.jpg"/></a> | Buyer - | Bid: - |
| Lot 834 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/834.jpg"/></a> | Buyer - | Bid: - |
| Lot 835 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/835.jpg"/></a> | Buyer - | Bid: - |
| Lot 836 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/836.jpg"/></a> | Buyer - | Bid: - |
| Lot 837 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/837.jpg"/></a> | Buyer - | Bid: - |
| Lot 838 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/838.jpg"/></a> | Buyer - | Bid: - |
| Lot 839 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/839.jpg"/></a> | Buyer - | Bid: - |
| Lot 840 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/840.jpg"/></a> | Buyer - | Bid: - |
| Lot 841 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/841.jpg"/></a> | Buyer - | Bid: - |
| Lot 842 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/842.jpg"/></a> | Buyer - | Bid: - |
| Lot 843 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/843.jpg"/></a> | Buyer - | Bid: - |
| Lot 844 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/844.jpg"/></a> | Buyer - | Bid: - |
| Lot 845 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/845.jpg"/></a> | Buyer - | Bid: - |
| Lot 846 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/846.jpg"/></a> | Buyer - | Bid: - |
| Lot 847 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/847.jpg"/></a> | Buyer - | Bid: - |
| Lot 848 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/848.jpg"/></a> | Buyer - | Bid: - |
| Lot 849 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/849.jpg"/></a> | Buyer - | Bid: - |
| Lot 850 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/850.jpg"/></a> | Buyer - | Bid: - |
| Lot 851 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/851.jpg"/></a> | Buyer - | Bid: - |
| Lot 852 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/852.jpg"/></a> | Buyer - | Bid: - |
| Lot 853 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/853.jpg"/></a> | Buyer - | Bid: - |
| Lot 854 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/854.jpg"/></a> | Buyer - | Bid: - |
| Lot 855 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/855.jpg"/></a> | Buyer - | Bid: - |
| Lot 856 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/856.jpg"/></a> | Buyer - | Bid: - |
| Lot 857 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/857.jpg"/></a> | Buyer - | Bid: - |
| Lot 858 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/858.jpg"/></a> | Buyer - | Bid: - |
| Lot 859 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/859.jpg"/></a> | Buyer - | Bid: - |
| Lot 860 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/860.jpg"/></a> | Buyer - | Bid: - |
| Lot 861 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/861.jpg"/></a> | Buyer - | Bid: - |
| Lot 862 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/862.jpg"/></a> | Buyer - | Bid: - |
| Lot 863 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/863.jpg"/></a> | Buyer - | Bid: - |
| Lot 864 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/864.jpg"/></a> | Buyer - | Bid: - |
| Lot 865 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/865.jpg"/></a> | Buyer - | Bid: - |
| Lot 866 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/866.jpg"/></a> | Buyer - | Bid: - |
| Lot 867 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/867.jpg"/></a> | Buyer - | Bid: - |
| Lot 868 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/868.jpg"/></a> | Buyer - | Bid: - |
| Lot 869 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/869.jpg"/></a> | Buyer - | Bid: - |
| Lot 870 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/870.jpg"/></a> | Buyer - | Bid: - |
| Lot 871 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/871.jpg"/></a> | Buyer - | Bid: - |
| Lot 872 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/872.jpg"/></a> | Buyer - | Bid: - |
| Lot 873 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/873.jpg"/></a> | Buyer - | Bid: - |
| Lot 874 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/874.jpg"/></a> | Buyer - | Bid: - |
| Lot 875 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/875.jpg"/></a> | Buyer - | Bid: - |
| Lot 876 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/876.jpg"/></a> | Buyer - | Bid: - |
| Lot 877 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/877.jpg"/></a> | Buyer - | Bid: - |
| Lot 878 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/878.jpg"/></a> | Buyer - | Bid: - |
| Lot 879 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/879.jpg"/></a> | Buyer - | Bid: - |
| Lot 880 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/880.jpg"/></a> | Buyer - | Bid: - |
| Lot 881 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/881.jpg"/></a> | Buyer - | Bid: - |
| Lot 882 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/882.jpg"/></a> | Buyer - | Bid: - |
| Lot 883 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/883.jpg"/></a> | Buyer - | Bid: - |
| Lot 884 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/884.jpg"/></a> | Buyer - | Bid: - |
| Lot 885 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/885.jpg"/></a> | Buyer - | Bid: - |
| Lot 886 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/886.jpg"/></a> | Buyer - | Bid: - |
| Lot 887 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/887.jpg"/></a> | Buyer - | Bid: - |
| Lot 888 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/888.jpg"/></a> | Buyer - | Bid: - |
| Lot 889 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/889.jpg"/></a> | Buyer - | Bid: - |
| Lot 890 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/890.jpg"/></a> | Buyer - | Bid: - |
| Lot 891 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/891.jpg"/></a> | Buyer - | Bid: - |
| Lot 892 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/892.jpg"/></a> | Buyer - | Bid: - |
| Lot 893 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/893.jpg"/></a> | Buyer - | Bid: - |
| Lot 894 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/894.jpg"/></a> | Buyer - | Bid: - |
| Lot 895 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/895.jpg"/></a> | Buyer - | Bid: - |
| Lot 896 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/896.jpg"/></a> | Buyer - | Bid: - |
| Lot 897 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/897.jpg"/></a> | Buyer - | Bid: - |
| Lot 898 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/898.jpg"/></a> | Buyer - | Bid: - |
| Lot 899 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/899.jpg"/></a> | Buyer - | Bid: - |
| Lot 900 | <a href="None"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/900.jpg"/></a> | Buyer - | Bid: - |

[Prev](items-8.md) | [1](index.md) | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [7](items-7.md) | [8](items-8.md) | **9** | [10](items-10.md) | [Next](items-10.md)
