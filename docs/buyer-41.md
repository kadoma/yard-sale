# Buyer 41's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 28 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot28.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/28.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $350.00 USD | Highest bid: $350.00 USD |
| Lot 151 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot151.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/151.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $260.00 USD | Highest bid: $270.00 USD |
| Lot 167 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot167.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/167.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $130.00 USD | Highest bid: $140.00 USD |
| Lot 189 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot189.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/189.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 217 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot217.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/217.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $45.00 USD | Highest bid: $45.00 USD |
| Lot 259 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot259.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/259.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $40.00 USD |
| Lot 262 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot262.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/262.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
