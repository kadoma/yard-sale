# Buyer 21's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 4 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot4.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/4.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $420.00 USD | Highest bid: $420.00 USD |
| Lot 8 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot8.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/8.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $80.00 USD | Highest bid: $95.00 USD |
| Lot 21 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot21.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/21.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
| Lot 31 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot31.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/31.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $20.00 USD |
| Lot 53 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot53.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/53.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $90.00 USD | Highest bid: $100.00 USD |
| Lot 149 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot149.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/149.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $30.00 USD |
| Lot 150 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot150.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/150.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $160.00 USD |
| Lot 178 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot178.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/178.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $100.00 USD | Highest bid: $190.00 USD |
| Lot 188 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot188.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/188.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $60.00 USD | Highest bid: $175.00 USD |
| Lot 194 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot194.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/194.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $15.00 USD | Highest bid: $15.00 USD |
| Lot 200 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot200.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/200.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 203 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot203.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/203.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $45.00 USD |
| Lot 213 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot213.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/213.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $120.00 USD | Highest bid: $170.00 USD |
| Lot 253 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot253.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/253.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $101.00 USD |
| Lot 349 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot349.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/349.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $160.00 USD | Highest bid: $200.00 USD |
| Lot 406 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot406.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/406.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $40.00 USD |
| Lot 429 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot429.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/429.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $30.00 USD |
| Lot 431 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot431.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/431.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $20.00 USD |
