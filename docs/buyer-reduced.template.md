# Buyer {buyer_number}

| ID   | Bid status | Your bid | Highest bid |
| ---- | ---------- | -------- | ----------- |
{bids}

[Back to top](#buyer-1)
