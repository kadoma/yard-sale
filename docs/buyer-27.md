# Buyer 27's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 38 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot38.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/38.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $120.00 USD | Highest bid: $120.00 USD |
| Lot 53 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot53.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/53.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $100.00 USD | Highest bid: $100.00 USD |
| Lot 137 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot137.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/137.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $120.00 USD | Highest bid: $125.00 USD |
| Lot 187 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot187.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/187.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $200.00 USD | Highest bid: $200.00 USD |
| Lot 221 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot221.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/221.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $1450.00 USD | Highest bid: $1500.00 USD |
| Lot 228 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot228.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/228.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
| Lot 244 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot244.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/244.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $60.00 USD | Highest bid: $60.00 USD |
| Lot 245 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot245.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/245.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $240.00 USD | Highest bid: $281.00 USD |
| Lot 250 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot250.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/250.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $70.00 USD | Highest bid: $70.00 USD |
| Lot 253 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot253.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/253.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $101.00 USD |
| Lot 326 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot326.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/326.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $32.00 USD |
| Lot 337 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot337.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/337.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $220.00 USD | Highest bid: $220.00 USD |
| Lot 349 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot349.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/349.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $140.00 USD | Highest bid: $200.00 USD |
| Lot 359 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot359.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/359.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $65.00 USD | Highest bid: $65.00 USD |
| Lot 428 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot428.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/428.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 455 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot455.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/455.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $70.00 USD | Highest bid: $70.00 USD |
| Lot 457 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot457.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/457.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $75.00 USD | Highest bid: $75.00 USD |
| Lot 458 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot458.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/458.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $40.00 USD | Highest bid: $40.00 USD |
