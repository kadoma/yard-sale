# Buyer 16's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 4 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot4.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/4.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $300.00 USD | Highest bid: $420.00 USD |
| Lot 5 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot5.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/5.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $2800.00 USD | Highest bid: $2950.00 USD |
| Lot 23 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot23.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/23.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 24 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot24.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/24.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $22.00 USD | Highest bid: $26.00 USD |
| Lot 25 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot25.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/25.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $25.00 USD | Highest bid: $25.00 USD |
| Lot 30 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot30.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/30.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $55.00 USD | Highest bid: $100.00 USD |
| Lot 32 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot32.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/32.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $38.00 USD | Highest bid: $45.00 USD |
| Lot 39 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot39.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/39.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 124 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot124.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/124.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $23.00 USD | Highest bid: $30.00 USD |
| Lot 177 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot177.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/177.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $600.00 USD | Highest bid: $1100.00 USD |
| Lot 237 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot237.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/237.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $17.00 USD | Highest bid: $20.00 USD |
| Lot 250 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot250.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/250.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $70.00 USD |
| Lot 290 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot290.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/290.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $17.00 USD | Highest bid: $45.00 USD |
| Lot 328 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot328.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/328.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 413 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot413.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/413.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $50.00 USD |
| Lot 417 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot417.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/417.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $45.00 USD | Highest bid: $110.00 USD |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $37.00 USD | Highest bid: $80.00 USD |
| Lot 492 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot492.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/492.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $12.00 USD |
