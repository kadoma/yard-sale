# Buyer 36's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 36 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot36.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/36.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $10.00 USD |
| Lot 113 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot113.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/113.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $35.00 USD | Highest bid: $35.00 USD |
| Lot 361 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot361.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/361.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $12.00 USD | Highest bid: $14.00 USD |
| Lot 385 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot385.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/385.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $30.00 USD |
| Lot 399 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot399.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/399.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $12.00 USD | Highest bid: $12.00 USD |
| Lot 400 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot400.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/400.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 416 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot416.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/416.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 512 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot512.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/512.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $26.00 USD |
