# Buyer {buyer_number}

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
{bids}

[Back to top](#buyer-1)
