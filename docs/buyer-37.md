# Buyer 37's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 1 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot1.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/1.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $310.00 USD | Highest bid: $100000.00 USD |
| Lot 26 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot26.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/26.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $36.00 USD |
| Lot 144 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot144.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/144.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $10.00 USD | Highest bid: $30.00 USD |
| Lot 153 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot153.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/153.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 176 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot176.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/176.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $560.00 USD | Highest bid: $620.00 USD |
| Lot 245 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot245.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/245.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $225.00 USD | Highest bid: $281.00 USD |
| Lot 395 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot395.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/395.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $22.00 USD | Highest bid: $22.00 USD |
| Lot 403 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot403.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/403.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $3.00 USD | Highest bid: $3.00 USD |
| Lot 405 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot405.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/405.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $2.00 USD | Highest bid: $2.00 USD |
