# Buyer 15's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 274 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot274.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/274.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $10.00 USD |
| Lot 297 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot297.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/297.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $35.00 USD |
| Lot 329 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot329.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/329.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $5.00 USD | Highest bid: $5.00 USD |
| Lot 397 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot397.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/397.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $290.00 USD | Highest bid: $380.00 USD |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $80.00 USD |
| Lot 476 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot476.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/476.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $22.00 USD | Highest bid: $50.00 USD |
