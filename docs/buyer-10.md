# Buyer 10's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 30 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot30.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/30.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $75.00 USD | Highest bid: $100.00 USD |
| Lot 130 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot130.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/130.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $50.00 USD |
| Lot 167 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot167.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/167.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $95.00 USD | Highest bid: $140.00 USD |
| Lot 242 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot242.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/242.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
| Lot 250 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot250.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/250.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $70.00 USD |
| Lot 413 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot413.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/413.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $35.00 USD | Highest bid: $50.00 USD |
| Lot 420 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot420.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/420.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $80.00 USD |
