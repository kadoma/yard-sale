# Buyer 13's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 73 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot73.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/73.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $15.00 USD | Highest bid: $32.00 USD |
| Lot 136 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot136.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/136.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $42.00 USD |
| Lot 148 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot148.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/148.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $20.00 USD | Highest bid: $70.00 USD |
| Lot 245 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot245.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/245.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $105.00 USD | Highest bid: $281.00 USD |
| Lot 251 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot251.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/251.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $65.00 USD | Highest bid: $220.00 USD |
| Lot 334 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot334.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/334.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $50.00 USD | Highest bid: $120.00 USD |
| Lot 337 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot337.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/337.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $55.00 USD | Highest bid: $220.00 USD |
| Lot 504 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot504.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/504.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $25.00 USD | Highest bid: $30.00 USD |
