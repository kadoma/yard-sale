# Items

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here to see check the status of bids where only the lots you have bid on are displayed**](buyers.md)</font></center>

<center>**To place a bid or make an appointment to view the items for sale, please phone 0773466270.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

**1** | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [Next](items-2.md)

| ID   | Picture | Buyer number | Highest bid |
| ---- | ------- | ------------ | ----------- |
| Lot 1 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot1.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/1.jpg"/></a> | Buyer - | Bid: - |
| Lot 2 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot2.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/2.jpg"/></a> | Buyer - | Bid: - |
| Lot 3 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot3.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/3.jpg"/></a> | Buyer - | Bid: - |
| Lot 4 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot4.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/4.jpg"/></a> | Buyer - | Bid: - |
| Lot 5 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot5.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/5.jpg"/></a> | Buyer - | Bid: - |
| Lot 6 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot6.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/6.jpg"/></a> | Buyer - | Bid: - |
| Lot 7 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot7.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/7.jpg"/></a> | Buyer - | Bid: - |
| Lot 8 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot8.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/8.jpg"/></a> | Buyer - | Bid: - |
| Lot 9 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot9.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/9.jpg"/></a> | Buyer - | Bid: - |
| Lot 10 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot10.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/10.jpg"/></a> | Buyer - | Bid: - |
| Lot 11 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot11.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/11.jpg"/></a> | Buyer - | Bid: - |
| Lot 12 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot12.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/12.jpg"/></a> | Buyer - | Bid: - |
| Lot 13 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot13.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/13.jpg"/></a> | Buyer - | Bid: - |
| Lot 14 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot14.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/14.jpg"/></a> | Buyer - | Bid: - |
| Lot 15 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot15.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/15.jpg"/></a> | Buyer - | Bid: - |
| Lot 16 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot16.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/16.jpg"/></a> | Buyer - | Bid: - |
| Lot 17 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot17.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/17.jpg"/></a> | Buyer - | Bid: - |
| Lot 18 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot18.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/18.jpg"/></a> | Buyer - | Bid: - |
| Lot 19 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot19.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/19.jpg"/></a> | Buyer - | Bid: - |
| Lot 20 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot20.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/20.jpg"/></a> | Buyer - | Bid: - |
| Lot 21 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot21.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/21.jpg"/></a> | Buyer - | Bid: - |
| Lot 22 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot22.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/22.jpg"/></a> | Buyer - | Bid: - |
| Lot 23 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot23.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/23.jpg"/></a> | Buyer - | Bid: - |
| Lot 24 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot24.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/24.jpg"/></a> | Buyer - | Bid: - |
| Lot 25 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot25.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/25.jpg"/></a> | Buyer - | Bid: - |
| Lot 26 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot26.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/26.jpg"/></a> | Buyer - | Bid: - |
| Lot 27 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot27.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/27.jpg"/></a> | Buyer - | Bid: - |
| Lot 28 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot28.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/28.jpg"/></a> | Buyer - | Bid: - |
| Lot 29 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot29.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/29.jpg"/></a> | Buyer - | Bid: - |
| Lot 30 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot30.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/30.jpg"/></a> | Buyer - | Bid: - |
| Lot 31 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot31.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/31.jpg"/></a> | Buyer - | Bid: - |
| Lot 32 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot32.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/32.jpg"/></a> | Buyer - | Bid: - |
| Lot 33 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot33.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/33.jpg"/></a> | Buyer - | Bid: - |
| Lot 34 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot34.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/34.jpg"/></a> | Buyer - | Bid: - |
| Lot 35 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot35.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/35.jpg"/></a> | Buyer - | Bid: - |
| Lot 36 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot36.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/36.jpg"/></a> | Buyer - | Bid: - |
| Lot 37 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot37.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/37.jpg"/></a> | Buyer - | Bid: - |
| Lot 38 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot38.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/38.jpg"/></a> | Buyer - | Bid: - |
| Lot 39 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot39.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/39.jpg"/></a> | Buyer - | Bid: - |
| Lot 40 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot40.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/40.jpg"/></a> | Buyer - | Bid: - |
| Lot 41 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot41.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/41.jpg"/></a> | Buyer - | Bid: - |
| Lot 42 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot42.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/42.jpg"/></a> | Buyer - | Bid: - |
| Lot 43 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot43.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/43.jpg"/></a> | Buyer - | Bid: - |
| Lot 44 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot44.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/44.jpg"/></a> | Buyer - | Bid: - |
| Lot 45 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot45.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/45.jpg"/></a> | Buyer - | Bid: - |
| Lot 46 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot46.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/46.jpg"/></a> | Buyer - | Bid: - |
| Lot 47 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot47.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/47.jpg"/></a> | Buyer - | Bid: - |
| Lot 48 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot48.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/48.jpg"/></a> | Buyer - | Bid: - |
| Lot 49 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot49.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/49.jpg"/></a> | Buyer - | Bid: - |
| Lot 50 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot50.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/50.jpg"/></a> | Buyer - | Bid: - |
| Lot 51 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot51.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/51.jpg"/></a> | Buyer - | Bid: - |
| Lot 52 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot52.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/52.jpg"/></a> | Buyer - | Bid: - |
| Lot 53 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot53.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/53.jpg"/></a> | Buyer - | Bid: - |
| Lot 54 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot54.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/54.jpg"/></a> | Buyer - | Bid: - |
| Lot 55 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot55.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/55.jpg"/></a> | Buyer - | Bid: - |
| Lot 56 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot56.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/56.jpg"/></a> | Buyer - | Bid: - |
| Lot 57 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot57.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/57.jpg"/></a> | Buyer - | Bid: - |
| Lot 58 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot58.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/58.jpg"/></a> | Buyer - | Bid: - |
| Lot 59 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot59.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/59.jpg"/></a> | Buyer - | Bid: - |
| Lot 60 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot60.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/60.jpg"/></a> | Buyer - | Bid: - |
| Lot 61 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot61.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/61.jpg"/></a> | Buyer - | Bid: - |
| Lot 62 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot62.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/62.jpg"/></a> | Buyer - | Bid: - |
| Lot 63 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot63.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/63.jpg"/></a> | Buyer - | Bid: - |
| Lot 64 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot64.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/64.jpg"/></a> | Buyer - | Bid: - |
| Lot 65 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot65.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/65.jpg"/></a> | Buyer - | Bid: - |
| Lot 66 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot66.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/66.jpg"/></a> | Buyer - | Bid: - |
| Lot 67 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot67.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/67.jpg"/></a> | Buyer - | Bid: - |
| Lot 68 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot68.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/68.jpg"/></a> | Buyer - | Bid: - |
| Lot 69 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot69.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/69.jpg"/></a> | Buyer - | Bid: - |
| Lot 70 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot70.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/70.jpg"/></a> | Buyer - | Bid: - |
| Lot 71 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot71.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/71.jpg"/></a> | Buyer - | Bid: - |
| Lot 72 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot72.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/72.jpg"/></a> | Buyer - | Bid: - |
| Lot 73 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot73.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/73.jpg"/></a> | Buyer - | Bid: - |
| Lot 74 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot74.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/74.jpg"/></a> | Buyer - | Bid: - |
| Lot 75 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot75.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/75.jpg"/></a> | Buyer - | Bid: - |
| Lot 76 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot76.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/76.jpg"/></a> | Buyer - | Bid: - |
| Lot 77 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot77.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/77.jpg"/></a> | Buyer - | Bid: - |
| Lot 78 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot78.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/78.jpg"/></a> | Buyer - | Bid: - |
| Lot 79 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot79.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/79.jpg"/></a> | Buyer - | Bid: - |
| Lot 80 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot80.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/80.jpg"/></a> | Buyer - | Bid: - |
| Lot 81 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot81.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/81.jpg"/></a> | Buyer - | Bid: - |
| Lot 82 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot82.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/82.jpg"/></a> | Buyer - | Bid: - |
| Lot 83 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot83.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/83.jpg"/></a> | Buyer - | Bid: - |
| Lot 84 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot84.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/84.jpg"/></a> | Buyer - | Bid: - |
| Lot 85 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot85.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/85.jpg"/></a> | Buyer - | Bid: - |
| Lot 86 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot86.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/86.jpg"/></a> | Buyer - | Bid: - |
| Lot 87 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot87.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/87.jpg"/></a> | Buyer - | Bid: - |
| Lot 88 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot88.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/88.jpg"/></a> | Buyer - | Bid: - |
| Lot 89 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot89.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/89.jpg"/></a> | Buyer - | Bid: - |
| Lot 90 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot90.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/90.jpg"/></a> | Buyer - | Bid: - |
| Lot 91 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot91.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/91.jpg"/></a> | Buyer - | Bid: - |
| Lot 92 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot92.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/92.jpg"/></a> | Buyer - | Bid: - |
| Lot 93 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot93.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/93.jpg"/></a> | Buyer - | Bid: - |
| Lot 94 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot94.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/94.jpg"/></a> | Buyer - | Bid: - |
| Lot 95 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot95.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/95.jpg"/></a> | Buyer - | Bid: - |
| Lot 96 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot96.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/96.jpg"/></a> | Buyer - | Bid: - |
| Lot 97 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot97.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/97.jpg"/></a> | Buyer - | Bid: - |
| Lot 98 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot98.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/98.jpg"/></a> | Buyer - | Bid: - |
| Lot 99 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot99.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/99.jpg"/></a> | Buyer - | Bid: - |
| Lot 100 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot100.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/100.jpg"/></a> | Buyer - | Bid: - |

**1** | [2](items-2.md) | [3](items-3.md) | [4](items-4.md) | [5](items-5.md) | [6](items-6.md) | [Next](items-2.md)
