# Buyer 24's bids

<center><font size="5">[**Click here to see the yard sale rules**](about.md)</font></center>

<center><font size="5">[**Click here for list of lots available for sale**](index.md)</font></center>

<center>**Here you can see whether or not you are still the highest bidder on each lot you have bid on. "Highest bid" means that you still have the highest bid, while "Outbid" means that you have been outbid by someone else. If you wish to increase your bids, you can call or message 0773466270.**</center>

<center>**If your bid is equal to the highest bid, but it shows that you have been outbid, it means someone else had placed a bid for that amount shortly before you did and before the page could be updated.**</center>

<center>**Please refresh the page periodically for up-to-date information.**</center>

| ID   | Picture | Bid status | Your bid | Highest bid |
| ---- | ------- | ---------- | -------- | ----------- |
| Lot 146 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot146.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/146.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $40.00 USD | Highest bid: $50.00 USD |
| Lot 151 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot151.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/151.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $225.00 USD | Highest bid: $270.00 USD |
| Lot 204 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot204.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/204.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $25.00 USD |
| Lot 205 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot205.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/205.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $5.00 USD | Highest bid: $80.00 USD |
| Lot 208 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot208.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/208.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $20.00 USD | Highest bid: $20.00 USD |
| Lot 213 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot213.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/213.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $170.00 USD | Highest bid: $170.00 USD |
| Lot 228 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot228.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/228.jpg"/></a> | <span style="color:firebrick">**Outbid**</span> | Your bid: $30.00 USD | Highest bid: $40.00 USD |
| Lot 231 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot231.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/231.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $30.00 USD | Highest bid: $30.00 USD |
| Lot 274 | <a href="https://u.cubeupload.com/KadomaYardSale/Lot274.jpeg"><img src="https://gitlab.com/kadoma/yard-sale/-/raw/master/thumbs/274.jpg"/></a> | <span style="color:green">**Highest bid**</span> | Your bid: $10.00 USD | Highest bid: $10.00 USD |
