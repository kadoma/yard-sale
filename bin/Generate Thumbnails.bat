@echo off

set PYTHON=py -3
cd ..\
%PYTHON% update_thumbnails.py
git add thumbs
git commit -m "Update thumbnails %TIME%"
git push

pause
