@echo off

set PYTHON=py -3
cd ..\
%PYTHON% update_full.py
git add docs
git add *.xlsx
git add thumbs
git commit -m "Full update %TIME%"
git push

pause
