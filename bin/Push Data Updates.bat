@echo off

set PYTHON=py -3
cd ..\
%PYTHON% update_pages.py
git add docs
git add *.xlsx
git commit -m "Update data %TIME%"
git push

pause
